"""
fit_model
---------

Likelihood function and other fittig functions to data

Notes
------

History

2019-06-11 frankel
2019-06-24 frankel copy from test_fit and clean
"""

# compute the survey volume integral of the model
# using pre-generated samples in a particular field
# and fit everything

# Import modules
import numpy as np
import matplotlib.pyplot as plt
import rm_new as rm
import useful_functions as uf
import time
from scipy.optimize import minimize

# Make enrichment
element = rm.element()

##################################################################
# define the model
##################################################################

#================================================================
# Model in action space for the disk
#================================================================

# in function of birth radius:
def global_model_R0(Lz, Jr, z, t, R0, pm, frc=False, 
                    return_components=False):
    """
    Global model for the galactic disk
    Extended distribution functions

    Parameters
    ----------
    Lz: array
        angular momentum
    JR: array
        radial action
    z: array
        height above the plane [kpc]
    t: array
        age [Gyr]
    R0: array
        birth radius [kpc]
    pm: class instance
        model parameters object
    el_name: list of strings
        ['element_name'] as named in enrichment param
    frc:boolean
        set to False by default

    Returns
    -------
    array
        global model

    Notes
    -----


    History:


    2019-05-12 frankel
    """
    # correct for the double model parameter
    pm.t_T = pm.t_old
    pm.t8 = pm.t_old

    #-----------------------------------------------------
    # calculate each model aspect
    #-----------------------------------------------------

    # Age and normalization constant
    p_age = rm.SFH_R_1(t, R0, pm, tmin=0.1)

    # Birth radius distribution
    p_R0 = rm.p_birth(R0, pm)*R0/pm.rd
    p_R0[R0 < 0] = 0

    # Angular momentum from radial migration
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0         # angular momentum of cold population
    p_migr = rm.p_migration_drift(Lz, t, Lz0, vc0, pm)

    # Radial action from heating          
    Rc = rm.R_circ(Lz)                   # guiding radius
    k = rm.epicycle_frequency(Rc)        # epicycle freq
    sigma_r = rm.vel_disp_r(Rc, t, pm)   # vel disp
    p_Jr = rm.p_heating(Jr, k, sigma_r)  # radial action dist

    # vertical profile from Ting
    pz = rm.p_z(Rc, R0, t, z, az=pm.a, pm=pm)

    #-------------------------------------------------------
    # put them together
    #-------------------------------------------------------
    p_model = p_age*p_R0*p_migr*p_Jr*pz

    if frc == True:
        p_model *= rm.f_RC(t)

    if return_components == False:
        return p_model
    else:
        return p_model, p_age, p_R0, p_migr, p_Jr, pz

# in metallicity
def global_model(Lz, Jr, z, t, fe, pm, el_name, frc=False,
                 enrichment_prescription='gamma'):
    """
    Global model for the galactic disk
    Extended distribution functions

    Parameters
    ----------
    Lz: array
        angular momentum
    JR: array
        radial action
    z: array
        height above the plane [kpc]
    t: array
        age [Gyr]
    fe: array
        metallicity [dex]
    pm: class instance
        moddel parameters object
    el_name: list of strings
        ['element_name'] as named in enrichment param
    frc: boolean
        set to False by default

    Returns
    --------
    array
        global model

    Notes
    -----

    History:


    2019-06-03 frankel
    """
    # extract enrichment params, (works only for 1 element)
    param_element = getattr(pm, 'enrich_'+el_name[0])

    # birth radius from metallicity
    if enrichment_prescription == 'gamma':
        R0 = element.R0_element(fe, t, param_element)
        m0 = (R0 + 0 <= 0)
        mm = R0 > 0
        R0[m0] = 0.1
        jacobian = -1/param_element.grad

    elif enrichment_prescription == 'broken':
        R0 = element.R0_break(fe, t, param_element)
        m0 = (R0 + 0 <= 0)
        mm = R0 > 0
        R0[m0] = 0.1
        jacobian = -1/param_element.grad*np.ones(R0.shape)
        jacobian[(R0 < param_element.Rbreak)] = -1/param_element.grad_in

    # Add a new elif with your key word and the jacobian dR/dFe as above [KKK]


    p_model = global_model_R0(Lz, Jr, z, t, R0, pm, frc=frc)

    # multiply by the Jacobian dR0/dFe
    p_model *= jacobian

    p_model[m0] = 0  # some model values may have returned negative birth radii,
                     # we set the model to zero
                     # technically this should be a prior, but easier to plug it in here
    return p_model

#================================================================
# Model in data space for the survey (un-normalized)
#================================================================
def data_model_fe(Data, el_name, pm, frc=False, log=True, 
                        enrichment_prescription='gamma'):
    """
    model in the data coordinates
    (no selection function in there)

    Arguments:
        [D =  dist (kpc)
        Lz, Jr = azimuthal and radial actions
        t = age
        Fe]  = metallicity
        pm = model parameters object

    Returns
        p(l, b, D, vphi, vr, t, R0 | pm)

    2019-06-03
    """
    # unpack and convert
    Dx, zx, Lzx, Jrx, tx, fex = Data
    # prior
    if isinstance(Dx, float):
        if (Dx <= 0) or (vphix <= 0) or (tx < 0.1) or (tx > pm.t8)\
               or (R0x <= 0.0)\
               or (Dx > 8):
            p_model = -np.inf
    else:
        # calculate the global model
        p_model = global_model(Lzx, Jrx, zx, tx, fex, pm, el_name, 
                  frc=False, enrichment_prescription=enrichment_prescription)

        if log == True:
            if np.any(p_model1 == 0):
                p_model1 = - np.inf
            else:
                p_model1 = np.log(p_model)
        else: p_model1 = np.copy(p_model)
    return p_model1

def data_model_fe_obs(Data, p_obs, q_dist, el_name, pm, frc=False, log=False, 
                      enrichment_prescription='gamma'):
    """
    model in the data coordinates, accounting for uncertainties
    (no selection function in there)

    Parameters
    -----------
    Data: list of arrays
        contains D, z, Lz, Jr, t, feh: the measured 'observables'
        [D =  dist (kpc)
        Lz, Jr = azimuthal and radial actions --> ndarray(Nobs, N_MC)
        t = age (Nobs, N_MC)
        Fe]  = metallicity (Nobs,)
    p_obs: array
        values of the noise model
    q_dist: array
        values of the pdf that generated the data
    el_name: list of strings
        ['element_name']
    pm: class instance
        model parameters
    frc: boolean
        default: false
    log: boolean
        deflaut: false
    enrichment_prescription: string
        default: 'gamma' corresponds to the chemical enrichment model one wants to use,
        as listed in rm_new.element

    Returns
    --------
        array
        the model values marginalized over observational uncertainties
        int p(l, b, D, vphi, vr, t, R0 | pm) 
              x p_obs(vphi_obs, vr_obs, t_obs | vphi, vr, t) dvphi dvr dt


    Notes
    -----

    History:


    2019-06-24 frankel
    """
    # unpack and convert
    Dx, zx, Lzx, Jrx, tx, fex = Data
    # prior
    if isinstance(Dx, float):
        if (Dx <= 0) or (vphix <= 0) or (tx <= 0.1) or (tx > pm.t8)\
               or (R0x <= 0.01)\
               or (Dx > 5):
            p_model = -np.inf
    else:
        # calculate the global model
        p_model = global_model(Lzx, Jrx, zx, tx, fex, pm, el_name, frc=False,
                            enrichment_prescription=enrichment_prescription)

        # multiply by uncertainty pdf and divide by sampling distribution
        p_model *= p_obs / q_dist

        # sum over the true value axis (integrate over true stars)
        p_model = np.sum(p_model, axis=1)/Lzx.shape[1]

        if log == True:
            if np.any(p_model1 == 0):
                p_model1 = - np.inf
            else:
                p_model1 = np.log(p_model)
        else: p_model1 = np.copy(p_model)
    return p_model1


# importance sampling (reweight the integral)
def Integral(p_target, p_samples, C_u_inv=1):
    C = np.sum(C_u_inv/p_samples)#/len(p_samples)
    IIi = p_target / p_samples
    II = np.sum(IIi)
    return II/C

def compute_variance(f, p_p):
    mu_i = 1#Integral(f, p_proposal)
    m = p_p <= 0
    print('zero ',np.sum(m), p_p.shape)
    w_i = 1#/p_p#np.divide(1,p_p)
    wi_i = 1#w_i/np.sum(w_i)
#    fmu_i = f - mu_i
#    var_i = wi_i**2 * fmu_i**2
#    variance = np.sum(var_i)/len(var_i)
    return 1#variance

def compute_effective_sample_size(w):
    n = len(w)
    w_avg = np.mean(w)
    w2_avg = np.mean(w**2)
    ne = w_avg**2/w2_avg#*n
    return ne


def likelihood(pm_array, pm_names, data_s, data_norm, pm_samples, 
               pm_object, el_name=None, frc=False, enrichment_prescription='gamma'):

    # update the parameters
    pm_object.update(pm_names, pm_array)

    # These lines compute the Survey volume integral ---> not needed for chemical enrichment (21/04/2021)
    # distributionon the samples for normalization  
    #p_samples = data_model_R0(data_norm,pm_samples,
    #                            frc=frc, log=False)

    # distribution of the model (numerator) for likelihood
    p_mod = data_model_fe(data_s, el_name, pm_object, frc=frc, log=False,
                        enrichment_prescription=enrichment_prescription)

    # distribution of the samples for normalization new model
    p_model_norm = data_model_R0(data_norm, pm_object, frc=frc,log=False)


    # These lines compute the Survey volume integral ---> not needed for chemical enrichment (21/04/2021)
    # compute the integral
    #I_mod = Integral(p_model_norm, p_samples)

    # normalize the model
    #p_mod /= I_mod

    if np.any(p_mod <= 0): log_p_mod = - np.inf # (near zero) negative probability very rarely occured due to numerical errors
                                                # or unrealistic prameters or data values
    else: log_p_mod = np.log(p_mod)

    # sum
    s_log_p_mod = np.sum(log_p_mod)

    if pm_object.c4 < 0: s_log_p_mod = -np.inf
    return s_log_p_mod


def likelihood_obs(pm_array, pm_names, data_s, data_norm, 
      p_obs, q_dist, p_samples, pm_object, el_name=None, frc=False,
      enrichment_prescription='gamma'):

    """
    Compute the likelihood of having the observed data drawn 
    from the model parameters pm_object

    Parameters
    -----------
    pm_array: array
        values of the model parameters to fit (or to evaluate the likelihood for)
    pm_names: list of strings
        keys corresponding to the model parameters in pm_array (to initialize the 
        model parameters object with these). One may decide to fit only for a
        subset of the overal model parameters so this is handy: just give a list
        of those you want to fit.
    data_s: list of arrays
        data to fit: [D, z, Lz, Jr, t, feh] with 
        D: distance (kpc)
        z: height above the midplane (kpc)
        Lz: angular momentum (kpc km/s)
        Jr: radial action (kpc km/s)
        t: Age (Gyr)
        feh: [Fe/H] or other element, [dex]
    data_norm: arrays
        samples used to compute the normalizing integral (Survey volume)
        Not needed if all model parameters related to SFR, structure, or dynamics are fixed
    p_obs: array
        noise model evaluated on the data points of data_s = p_obs(data_s)
    q_dist: array
        values of the pdf that generated the samples used to marginalize
        over observational uncertainties = q_obs(data_s)
    p_samples: array
        values of the model on the samples used to compute the survey volume
        Not needed if all model parameters are fixed
    pm_object: class instance
        model parameters object, that is associated with pm_array and pm_names
    el_name: list of strings
        name of the elements to fit for ['Fe']
    frc: boolean
        default: False
    enrichment_prescription: string
        default: 'gamma'
        chemical enrichment prescription as coded in rm_new.element ('expo', 'Weinberg')


    Returns
    --------
    float
        likelihood evaluated on the model parameter pm_names in pm_array


    Notes
    ------


    History:

    2019-06-24 created frankel

    2021-04-21 commented out the selection function-related lines frankel
    """
    # update the parameters
    pm_object.update(pm_names, pm_array)

    # distribution of the model (numerator) for likelihood
    p_mod = data_model_fe_obs(data_s, p_obs, q_dist, 
           el_name, pm_object, frc=frc, log=False,
           enrichment_prescription=enrichment_prescription)

    # 2021-04-21: not useful for chemical enrichment
    # distribution of the samples for normalization new model 
    # p_model_norm = data_model_R0(data_norm, pm_object, 
    #                              frc=frc,log=False)

    # compute the integral
    # I_mod = Integral(p_model_norm, p_samples)

    # normalize the model
    # p_mod /= I_mod

    # take the log
    if np.any(p_mod <= 0): log_p_mod = - np.inf # rarely occurs for unrealistic values of parameteres or data
    else: log_p_mod = np.log(p_mod)

    # sum
    s_log_p_mod = np.sum(log_p_mod)

    return s_log_p_mod


def minus_likelihood(pm_array, pm_names, data_s, data_norm, 
                pm_samples, pm_object, el_name=None, frc=False,
                enrichment_prescription='gamma'):
    """
    Minus likelihood: the quantity to minimize to get the best fit parameters
    Not accounting for data uncertainties

    Parameters
    -----------
    pm_array: array
        values of the model parameters to fit (or to evaluate the likelihood for)
    pm_names: list of strings
        keys corresponding to the model parameters in pm_array (to initialize the 
        model parameters object with these). One may decide to fit only for a
        subset of the overal model parameters so this is handy: just give a list
        of those you want to fit.
    data_s: list of arrays
        data to fit: [D, z, Lz, Jr, t, feh] with 
        D: distance (kpc)
        z: height above the midplane (kpc)
        Lz: angular momentum (kpc km/s)
        Jr: radial action (kpc km/s)
        t: Age (Gyr)
        feh: [Fe/H] or other element, [dex]
    data_norm: arrays
        samples used to compute the normalizing integral (Survey volume)
        Not needed if all model parameters related to SFR, structure, or dynamics are fixed
    pm_object: class instance
        model parameters object, that is associated with pm_array and pm_names
    el_name: list of strings
        name of the elements to fit for ['Fe']
    frc: boolean
        default: False
    enrichment_prescription: string
        default: 'gamma'
        chemical enrichment prescription as coded in rm_new.element ('expo', 'Weinberg')

    """
    p= - likelihood(pm_array, pm_names, data_s, data_norm, 
                pm_samples, pm_object, el_name=el_name, frc=frc,
                enrichment_prescription=enrichment_prescription)
    print(pm_array, p)
    return p


def minus_likelihood_obs(pm_array, pm_names, data_s, data_norm,
                p_obs, q_dist, p_samples, pm_object, el_name=None, 
                frc=False, enrichment_prescription='gamma'):
    """
    minus likelihood to minimize (different from the non obs version:
          have to precompute the model on the normalization samples

    Parameters
    -----------
    pm_array: array
        values of the model parameters to fit (or to evaluate the likelihood for)
    pm_names: list of strings
        keys corresponding to the model parameters in pm_array (to initialize the 
        model parameters object with these). One may decide to fit only for a
        subset of the overal model parameters so this is handy: just give a list
        of those you want to fit.
    data_s: list of arrays
        data to fit: [D, z, Lz, Jr, t, feh] with 
        D: distance (kpc)
        z: height above the midplane (kpc)
        Lz: angular momentum (kpc km/s)
        Jr: radial action (kpc km/s)
        t: Age (Gyr)
        feh: [Fe/H] or other element, [dex]
    data_norm: arrays
        samples used to compute the normalizing integral (Survey volume)
        Not needed if all model parameters related to SFR, structure, or dynamics are fixed
    p_obs: array
        noise model evaluated on the data points of data_s = p_obs(data_s)
    q_dist: array
        values of the pdf that generated the samples used to marginalize
        over observational uncertainties = q_obs(data_s)
    p_samples: array
        values of the model on the samples used to compute the survey volume
        Not needed if all model parameters are fixed
    pm_object: class instance
        model parameters object, that is associated with pm_array and pm_names
    el_name: list of strings
        name of the elements to fit for ['Fe']
    frc: boolean
        default: False
    enrichment_prescription: string
        default: 'gamma'
        chemical enrichment prescription as coded in rm_new.element ('expo', 'Weinberg')

    Returns
    -------
    float
        minus likelihood values

    Notes
    -----

    History

    2019-06-26 frankel
    """
    p= - likelihood_obs(pm_array, pm_names, data_s, data_norm,
         p_obs, q_dist, p_samples, pm_object, el_name=el_name, 
         frc=frc, enrichment_prescription=enrichment_prescription)

    # print the model paramters and likelihood values
    print(pm_array, p)

    return p


def posterior(pm_array, pm_names, pm_min, pm_max, 
              data_s, data_norm, pm_samples, pm_object,
              el_name=None, frc=False, enrichment_prescription='gamma'):
    """
    Posterior of the parameter pdf
    """
    p = -np.inf
    if np.all(pm_array >= pm_min) & np.all(pm_array <= pm_max):
        p = likelihood(pm_array, pm_names, 
            data_s, data_norm, pm_samples,
            pm_object, el_name=el_name, frc=frc,
            enrichment_prescription=enrichment_prescription)
    return p

def posterior_obs(pm_array, pm_names, pm_min, pm_max,
              data_s, data_norm, p_obs, q_dist, p_samples, pm_object,
              el_name=None, frc=False, enrichment_prescription='gamma'):
    """
    Posterior of the parameter pdf



    Parameters
    -----------
    pm_array: array
        values of the model parameters to fit (or to evaluate the likelihood for)
    pm_names: list of strings
        keys corresponding to the model parameters in pm_array (to initialize the 
        model parameters object with these). One may decide to fit only for a
        subset of the overal model parameters so this is handy: just give a list
        of those you want to fit.
    pm_min: minimum values allowed for pm_array (acts like a prior)
    pm_max: maximum values allowed for pm_array (acts like a prior)
    data_s: list of arrays
        data to fit: [D, z, Lz, Jr, t, feh] with 
        D: distance (kpc)
        z: height above the midplane (kpc)
        Lz: angular momentum (kpc km/s)
        Jr: radial action (kpc km/s)
        t: Age (Gyr)
        feh: [Fe/H] or other element, [dex]
    data_norm: arrays
        samples used to compute the normalizing integral (Survey volume)
        Not needed if all model parameters related to SFR, structure, or dynamics are fixed
    p_obs: array
        noise model evaluated on the data points of data_s = p_obs(data_s)
    q_dist: array
        values of the pdf that generated the samples used to marginalize
        over observational uncertainties = q_obs(data_s)
    p_samples: array
        values of the model on the samples used to compute the survey volume
        Not needed if all model parameters are fixed
    pm_object: class instance
        model parameters object, that is associated with pm_array and pm_names
    el_name: list of strings
        name of the elements to fit for ['Fe']
    frc: boolean
        default: False
    enrichment_prescription: string
        default: 'gamma'
        chemical enrichment prescription as coded in rm_new.element ('expo', 'Weinberg')

    Returns
    -------
    float
        posterior


    """
    p = -np.inf
    if np.all(pm_array >= pm_min) & np.all(pm_array <= pm_max):
        p = likelihood_obs(pm_array, pm_names,
            data_s, data_norm, p_obs, q_dist, p_samples,
            pm_object, el_name=el_name, frc=frc,
            enrichment_prescription=enrichment_prescription)
    return p






