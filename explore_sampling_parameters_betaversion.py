import sample_data_kinematics_betaversion as sample
import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table
import useful_functions as uf
import rm_new as rm
import pickle
import random
import emcee

#-----------------------------------------------------------------------
# load selection function
#-----------------------------------------------------------------------
# pure selection function
locs = uf.load_fields_lb(l_min=0, l_max=360, b_min=1, b_max=20)#[:100]

# the data in it
dataset_path = '../sub_proj_sel_z/data_cuts/Melissa_Bovy_RC_DR12.fits' 
#loc_id, h_min, h_max, Extinct_med, \
#                      sel_frac, radius, lfield, bfield, \
#                      l_d, b_d, D_d, Fe, age = uf.load_data_fields(locs,\
#                      dataset_path, short=True, med=False, Long=False, \
#                      age_min=0, age_max=12, feh_min=-3, feh_max=3)

#datasetallfields = uf.load_data_fields(locs,\
#                      dataset_path, short=True, med=False, Long=False, \
#                      age_min=0, age_max=12, feh_min=-3, feh_max=3)

#with open('data_all_disk_fields.pkl', 'wb') as output:
#    pickle.dump(datasetallfields, output, pickle.HIGHEST_PROTOCOL)

#with open('data_all_disk_fields.pkl', 'rb') as input:
#    datasetallfields = pickle.load(input)

#with open('data_trust_fields_2018-12.pkl', 'rb') as input:
#    datasetallfields = pickle.load(input)

#with open('data_2019_01_17_fields.pkl', 'rb') as input:
#    datasetallfields = pickle.load(input)


#loc_id, h_min, h_max, Extinct_med, \
#    sel_frac, radius, lfield, bfield, \
#    l_d, b_d, D_d, Fe, age = datasetallfields

#locs = np.copy(loc_id)
# load fields information
l_d, b_d, sel_frac, ak_med, hmin, hmax, radius_plate, t_i = \
    uf.load_info_fields(locs, short=True, med=False, Long=False)

print('Dmin, Dmax caluclated for RC, check the explore_sampling D_min = line')
D_min = uf.calculate_distance(hmin)
D_max = uf.calculate_distance(hmax)

plt.figure()
plt.hist(D_max,100)
plt.show()

#area = []
#for i in range(len(radius_plate)):
#    area.append((1 - np.cos(np.radians(radius_plate[i])))) 

#ldata = np.copy(l_d)
#bdata = np.copy(b_d)

#=========================================================================
# additional cuts ?
#=========================================================================
#m = np.abs(zdata < 0.2)
#ldata = np.concatenate(l_d)#[m]
#bdata = np.concatenate(b_d)#[m]
#Rdata = Rdata[m]
#agedata = agedata[m]
#zdata = zdata[m]

#=========================================================================
# calculate distance limits in the standard candle case
#=========================================================================
#D_min = uf.calculate_distance(np.array(h_min)) 
#D_max = uf.calculate_distance(np.array(h_max))
D_max[D_max > 5.5] = 5.5




#################################################################################
# Function that samples points given
# -- sampling parameters
# -- model parameters
# -- number of points
#################################################################################

def sample_points(pm_sample, pm_model, n, dust=False, 
              showfig=True, lbd=False, density=None):
    """
    Sample points given sampling parameters and model parameters

    Arguments:
    pm_sample = object of sampling distribution parameters
    pm_model = object of model parameters
    N = number of points in the highest selection fraction field

    Returns:
    Acceptance fraction
    """ 

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
    radius_plate = np.array(radius)
    l_p = np.array(lfield)
    b_p = np.array(bfield)
    selfr = np.array(sel_frac)
    selfrac = selfr

    #==========================================================================
    # draw samples
    #==========================================================================

    # draw positions 
    R, z, l, b, d, loc =  sample.draw_several_pointings_expo(n, l_p, b_p, \
                      D_min, D_max, pm_sample, radius_plate, area,selfrac,\
                     loc_id, dust=dust, return_maxF=False, density=density)

    # draw ages
    t = sample.sample_age(len(R), pm_sample)

    # draw velocities
    vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

    # draw their birth radii
    R0 = sample.draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_sampled = len(t)
    mask = (R0 > 0) & (vphi > 0)
    t = t[mask]
    vphi = vphi[mask]
    vr = vr[mask]
    vz = vz[mask]
    JR = JR[mask]
    LZ = LZ[mask]
    JZ = JZ[mask]
    vc0 = vc0[mask]
    Lz0 = Lz0[mask]
    R = R[mask]
    z = z[mask]
    l = l[mask]
    b = b[mask]
    d = d[mask]
    loc = loc[mask]
    R0 = R0[mask]

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
    p_birth = rm.p_birth_radius(R0, t, pm_model)*R0/pm_model.rd 

    # radial migration
    p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm_model)

    # heating
    Rcirc = rm.R_circ(LZ)
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t, pm_model)
    p_heating = rm.p_heating(JR, k, sigma_r) #+ 1e-500

    # vertical distribution
#    p_z = np.exp(-np.abs(z)/pm_sample.hz)/(2*pm_sample.hz)
#    hz = np.zeros(len(z))
#    hz[t < 4] = 0.5  #pm_sample.hz
#    hz[t >= 4] = 0.5 #pm_sample.hz*3
#    p_z = np.exp(-np.abs(z)/hz)/(2*hz)
    p_z = rm.p_z(R, R0, t, z)

    # cast all in together
    density_model = p_birth*p_migr*p_heating*p_z #+ 1e-500

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    if density is None:
        p_position = sample.rho(R, z, pm_sample)
    else:
        p_position = density(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # sample vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    #  asymmetric drift
#    A = (1./3.5 + 1./4.)*R - 0.5
#    vdrift = A*vel_disp**2/(2*vcirc)
    
    # vphi
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)

    # vR 
    p_vR = rm.gauss(0, vr, vel_disp)

    # birth radii
    sig = pm_sample.srm*np.sqrt(t/12)
    D = -pm_sample.srm**2/(2*pm_sample.rd*12)
    p_r0 = rm.gauss(R0, R + D*t, sig)

    # cast in  all together
    p_proposal = p_position*p_vphi*p_vR*p_r0

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model*R0 / p_proposal
    max_ratio = np.max(ratio)

    # keep the points with the correct probability
    p_uniform = np.random.rand(len(R))*max_ratio
    mask = p_uniform < ratio

    a = np.sum(mask)/N_sampled
#    print('Max ratio ', max_ratio)
#    print('Acceptrance fraction a = ', a)
    if showfig == True:
        print('Acceptrance fraction a = ', a)
        print('Number of points = ', np.sum(mask))
        mask_high = ratio > max_ratio / 10
        print('N highghest:', np.sum(mask_high))

        plt.figure()
        plt.scatter(LZ[mask_high], np.sqrt(JR[mask_high]), s=50, c=density_model[mask_high]/np.mean(density_model), alpha=1)
        plt.colorbar(label='density model')
        plt.xlabel('Lz')
        plt.ylabel('Jr')

        plt.figure()
        plt.scatter(LZ[mask_high], np.sqrt(JR[mask_high]), s=50, c=p_proposal[mask_high]/np.mean(p_proposal), alpha=1)
        plt.colorbar(label='proposal')
        plt.xlabel('Lz')
        plt.ylabel('Jr')

#        plt.scatter(LZ[mask], np.sqrt(JR[mask]), s=2, c=t[mask], alpha=0.1)
#        plt.xlabel('Lz')
#        plt.ylabel('Jr')
#        plt.show()
        plt.hist(R, density=1, alpha=0.5, bins=100, label='proposed')
        plt.hist(R[mask], density=1, bins=50, alpha=0.5, label='accepted')
        plt.legend()
        plt.xlabel('R [kpc]')

        plt.show()

    p_model_list = [p_birth[mask], p_migr[mask], p_heating[mask], p_z[mask]]
    p_proposal_list = [p_position[mask], p_vphi[mask], p_vR[mask], p_r0[mask]]

    if not lbd:
        return a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                  vphi[mask], vr[mask], p_model_list, p_proposal_list
    if lbd:
        return a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                 vphi[mask], vr[mask], p_model_list, p_proposal_list,\
                                                  l[mask], b[mask], d[mask], loc[mask]




def sample_points_in_given_fields(pm_sample, pm_model, n, fieldstuff, dust=False,
              showfig=True, lbd=False, density=None, frc=False, frc_t=0):
    """
    2019-06-09

    Sample points given sampling parameters and model parameters

    Arguments:
    pm_sample = object of sampling distribution parameters
    pm_model = object of model parameters
    n = number of points in the highest selection fraction field
    fieldstuff = list of [radius, lfield, bfield, selfrac, dmin, dmax]

    Returns:
    Acceptance fraction
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
    rrad, llfield, bbfield, ssfrac, dmin, dmax, area, locids = fieldstuff
    radius_plate = np.array(rrad)
    l_p = np.array(llfield)
    b_p = np.array(bbfield)
    selfr = np.array(ssfrac)
    selfrac = selfr

    #==========================================================================
    # draw samples
    #==========================================================================

    # draw positions 
    R, z, l, b, d, loc =  sample.draw_several_pointings_expo(n, l_p, b_p, \
                     dmin, dmax, pm_sample, radius_plate, area, selfrac,\
                     locids, dust=dust, return_maxF=False, density=density)

    nn = len(R)
    # draw ages
    #t = np.random.rand(nn)*12    # draw ages uniformly
    #sample.sample_age(len(R), pm_sample)
    t = sample.sample_age_IO(R, pm_sample, tmin=0.1, frc_t=frc_t)

    # draw velocities
    vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

    # draw their birth radii
    R0 = sample.draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_sampled = len(t)
    mask = (R0 > 0) & (vphi > 0)
    t = t[mask]
    vphi = vphi[mask]
    vr = vr[mask]
    vz = vz[mask]
    JR = JR[mask]
    LZ = LZ[mask]
    JZ = JZ[mask]
    vc0 = vc0[mask]
    Lz0 = Lz0[mask]
    R = R[mask]
    z = z[mask]
    l = l[mask]
    b = b[mask]
    d = d[mask]
    loc = loc[mask]
    R0 = R0[mask]

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
    p_birth = rm.p_birth(R0, pm_model)*R0/pm_model.rd

    # age distribution
#    c1 = rm.norm_c1_R_1(R0, 0.1, 12, pm_model, frc=False)
#    p_age = rm.p_age_R_1(t, R0, c1, pm_model, frc=False)
    p_age = rm.SFH_R_1(t, R0, pm_model, tmin=0.1)

    # radial migration
    p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm_model)

    # heating
    Rcirc = rm.R_circ(LZ)
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t, pm_model)
    p_heating = rm.p_heating(JR, k, sigma_r) #+ 1e-500

    # vertical distribution
    #p_z = rm.p_z(R, R0, t, z)
    p_z = r,m.p_z(Rcirc, R0, t, z)

    # cast all in together
    density_model = p_birth*p_migr*p_heating*p_z*p_age#*rm.f_RC(t) #+ 1e-500

    # possbibly select redclump stars
    if frc == True:
        f_select_RC = rm.f_RC(t)
        density_model *= f_select_RC

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    if density is None:
        p_position = sample.rho(R, z, pm_sample)
    else:
        p_position = density(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # sample vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)

    # vR 
    p_vR = rm.gauss(0, vr, vel_disp)

    # p_t
#    p_t = 1./12.
    p_t = sample.p_age_IO(t, R, pm_sample, tmin=0.1, frc_t=frc_t)

    # birth radii
    sig = pm_sample.srm*np.sqrt(t/12)
    D = -pm_sample.srm**2/(2*pm_sample.rd*12)
    p_r0 = rm.gauss(R0, R + D*t, sig)

    # cast in  all together
    p_proposal = p_position*p_vphi*p_vR*p_r0*p_t

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model / p_proposal
    max_ratio = np.max(ratio)

    # keep the points with the correct probability
    p_uniform = np.random.rand(len(R))*max_ratio
    mask = p_uniform < ratio

    a = np.sum(mask)/N_sampled


    print(pm_model.t8)
    p_model_list = [p_birth[mask], p_migr[mask], p_heating[mask], p_z[mask]]
    p_proposal_list = [p_position[mask], p_vphi[mask], p_vR[mask], p_r0[mask]]

    if not lbd:
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                  vphi[mask], vr[mask], p_model_list, p_proposal_list]
    if lbd:
        print(d.shape)
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                 vphi[mask], vr[mask], p_model_list, p_proposal_list,\
                                                  l[mask], b[mask], d[mask], loc[mask]]


def sample_points_in_given_fields_DR(pm_sample, pm_model, n, fieldstuff, dust=False,
              showfig=False, lbd=False, density=None, frc=False, 
              frc_t=0, radial_migration=True):
    """
    2019-08-21 

    update from the above to incude apogee-2 fields and be more general
           use all of the usual sampling functions but with the '_DR'
           subscript

    Sample points given sampling parameters and model parameters

    Arguments:
    pm_sample = object of sampling distribution parameters
    pm_model = object of model parameters
    n = number of points in the highest selection fraction field
    fieldstuff = list of [radius, lfield, bfield, selfrac, dmin, dmax]
    radial_migration = boolean. Default=True, if set: stars radial migrate
                       if not, they don't. The code will bebave differently as
                       rejection sampling on Dirac function is suicidal..

    Returns:
    Acceptance fraction

    2021-01-29 frankel: remove the p_age as now the age sampling is exact
                        (sample..IO in sample_data_kinematics_betaversion)
                        subscirpt '_old' = other version
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
    rrad, llfield, bbfield, ssfrac, d_array, frac_dust, area, locids = fieldstuff
    radius_plate = np.array(rrad)
    l_p = np.array(llfield)
    b_p = np.array(bbfield)
    selfr = np.array(ssfrac)
    selfrac = selfr

    #==========================================================================
    # draw samples
    #==========================================================================

    # draw positions ===
    R, z, l, b, d, loc, weight =  sample.draw_several_pointings_expo_DR(n, l_p, b_p, \
                     d_array, frac_dust, pm_sample, radius_plate, area, selfrac,\
                     locids, dust=dust, return_maxF=False, density=density)

    nn = len(R)

    # draw ages
    t = sample.sample_age_IO(R, pm_sample, tmin=0.1, frc_t=frc_t)

    # draw velocities
    vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

    # draw their birth radii
    R0 = sample.draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    if radial_migration:
        Lz0 = R0*vc0
    else: 
        Lz0 = LZ
        R0 = Lz0/vc0

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_sampled = len(t)
    mask = (R0 > 0) & (vphi > 0)
    t = t[mask]
    vphi = vphi[mask]
    vr = vr[mask]
    vz = vz[mask]
    JR = JR[mask]
    LZ = LZ[mask]
    JZ = JZ[mask]
    vc0 = vc0[mask]
    Lz0 = Lz0[mask]
    R = R[mask]
    z = z[mask]
    l = l[mask]
    b = b[mask]
    d = d[mask]
    loc = loc[mask]
    weight = weight[mask]
    R0 = R0[mask]

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
    p_birth = rm.p_birth(R0, pm_model)*R0/pm_model.rd

    # age distribution
    #p_age = rm.SFH_R_1(t, R0, pm_model, tmin=0.1)

    # radial migration
    if radial_migration:
        p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm_model)
    else: p_migr = 1 + 0*p_birth

    # heating
    Rcirc = rm.R_circ(LZ)
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t, pm_model)
    p_heating = rm.p_heating(JR, k, sigma_r)

    # vertical distribution
    p_z = rm.p_z(Rcirc, R0, t, z)

    # cast all in together
    density_model = p_birth*p_migr*p_heating*p_z#*p_age

    # possbibly select redclump stars
    if frc == True:
        f_select_RC = rm.f_RC(t)
        density_model *= f_select_RC

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    if density is None:
        p_position = sample.rho(R, z, pm_sample)
    else:
        p_position = density(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # sample vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)

    # vR 
    p_vR = rm.gauss(0, vr, vel_disp)

    # p_t
    #p_t = sample.p_age_IO(t, R, pm_sample, tmin=0.1, frc_t=frc_t)

    # birth radii
    if radial_migration:
        sig = pm_sample.srm*np.sqrt(t/12)
        D = -pm_sample.srm**2/(2*pm_sample.rd*12)
        p_r0 = rm.gauss(R0, R + D*t, sig)
    else:
        p_r0 = 1+ 0*p_vR

    # cast in  all together
    p_proposal = p_position*p_vphi*p_vR*p_r0#*p_t

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model / p_proposal
    max_ratio = np.max(ratio)

    # keep the points with the correct probability
    p_uniform = np.random.rand(len(R))*max_ratio
    mask = p_uniform < ratio
    a = np.sum(mask)/N_sampled

    p_model_list = [p_birth[mask], p_migr[mask], p_heating[mask], p_z[mask]]
    p_proposal_list = [p_position[mask], p_vphi[mask], p_vR[mask], p_r0[mask]]


    if showfig:
        mask_high = ratio > max_ratio / 10
        mask_low = ratio < max_ratio / 1000000
        print('N highghest:', np.sum(mask_high), np.sum(mask_low))

        plt.figure()
        plt.scatter(LZ[mask_high], np.sqrt(JR[mask_high]), s=50, c=density_model[mask_high]/np.mean(density_model), alpha=1)
        plt.scatter(LZ[mask_low], np.sqrt(JR[mask_low]), s=50, c=density_model[mask_low]/np.mean(density_model), alpha=1)

        plt.colorbar(label='density model')
        plt.xlabel('Lz')
        plt.ylabel('Jr')

        plt.figure()
        plt.scatter(LZ[mask_high], np.sqrt(JR[mask_high]), s=50, c=p_proposal[mask_high]/np.mean(p_proposal), alpha=1)
        plt.colorbar(label='proposal')
        plt.xlabel('Lz')
        plt.ylabel('Jr')


        plt.figure()
        plt.scatter(LZ[mask_high], np.sqrt(JR[mask_high]), s=50, c=ratio[mask_high]/max_ratio, alpha=1)
        plt.scatter(LZ[mask_low], np.sqrt(JR[mask_low]), s=50, c=ratio[mask_low]/max_ratio, alpha=1)
        plt.colorbar(label='ratio')
        plt.xlabel('Lz')
        plt.ylabel('Jr')

        plt.figure()
        plt.scatter(R0[mask_high], t[mask_high], s=50, c=ratio[mask_high]/max_ratio, alpha=1)
        plt.scatter(R0[mask_low], t[mask_low], s=50, c=ratio[mask_low]/max_ratio, alpha=1)
        plt.colorbar(label='ratio')
        plt.xlabel('R0')
        plt.ylabel('t')

        plt.figure()
        plt.scatter(R[mask_high], np.sqrt(z[mask_high]), s=50, c=ratio[mask_high]/max_ratio, alpha=1)
        plt.scatter(R[mask_low], np.sqrt(z[mask_low]), s=50, c=ratio[mask_low]/max_ratio, alpha=1)
        plt.colorbar(label='ratio')
        plt.xlabel('R')
        plt.ylabel('z')

        plt.figure()
        plt.scatter(t[mask_high], np.sqrt(vr[mask_high]), s=50, c=ratio[mask_high]/max_ratio, alpha=1)
        plt.scatter(t[mask_low], np.sqrt(vr[mask_low]), s=50, c=ratio[mask_low]/max_ratio, alpha=1)
        plt.colorbar(label='ratio')
        plt.xlabel('t')
        plt.ylabel('vR')

        plt.figure()
        plt.hist(R, 200, density=1, alpha=0.3)
        plt.hist(R[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(R[mask_low], density=1, alpha=0.3, label='low')
        plt.hist(R[mask], density=1, histtype='step', color='k', lw=2, bins=100)
        plt.xlabel('R')
        plt.legend()

        plt.figure()
        plt.hist(t, 200, density=1)
        plt.hist(t[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(t[mask_low], density=1, alpha=0.3, label='low')
        plt.hist(t[mask], density=1, histtype='step', color='k', lw=2, bins=100)
        plt.xlabel('t')
        plt.legend()

        plt.figure()
        plt.hist(R0, 200, density=1, alpha=0.3)
        plt.hist(R0[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(R0[mask_low], density=1, alpha=0.3, label='low')
        plt.hist(R0[mask], density=1, histtype='step', color='k', lw=2, bins=100)
        plt.xlabel('R0')
        plt.legend()

        plt.figure()
        plt.hist(LZ, 200, density=1, alpha=0.3)
        plt.hist(LZ[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(LZ[mask_low], density=1, alpha=0.3, label='low')
        plt.hist(LZ[mask], density=1,  histtype='step', color='k', lw=2, bins=100)
        plt.xlabel('Lz')
        plt.legend()

        plt.figure()
        plt.hist(JR, 200, density=1, alpha=0.3)
        plt.hist(JR[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(JR[mask_low], density=1, alpha=0.3, label='low')
        plt.legend()
        plt.xlabel('JR')

    if not lbd:
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                  vphi[mask], vr[mask], p_model_list, p_proposal_list]
    if lbd:
        print(d.shape)
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                 vphi[mask], vr[mask], p_model_list, p_proposal_list,\
                                    l[mask], b[mask], d[mask], loc[mask], weight[mask]]



def sample_points_at_given_R_DR(pm_sample, pm_model, n, Rgal, zmax=1,
        showfig=False, lbd=False, 
        density=None, frc=False, frc_t=0):
    """
    2020-09-09=

    sample from the model at given Galactocentric radius:
    p(Lz, ..., t, fe | Rgal)


    Sample points given sampling parameters and model parameters

    Arguments:
    pm_sample = object of sampling distribution parameters
    pm_model = object of model parameters
    n = number of points in the highest selection fraction field
    Rgal = galactocentric radius where to sample points
    zmax = maximum height to sample at (1 kpc default)

    Returns:
    Acceptance fraction
    """

    #==========================================================================
    # draw samples
    #==========================================================================

    # draw positions 
    #R, z, l, b, d, loc =  sample.draw_several_pointings_expo_DR(n, l_p, b_p, \
    #                 d_array, frac_dust, pm_sample, radius_plate, area, selfrac,\
    #                 locids, dust=dust, return_maxF=False, density=density)

    R = np.zeros(n) + Rgal
    z = np.random.exponential(size=n, scale=zmax)
    #theta = np.random.rand(n)*2*np.pi
    #x = np.cos(theta)*R
    #y = np.sin(theta)*R
    #x1, y1, z1


    nn = len(R)

    # draw ages
    t = sample.sample_age_IO(R, pm_sample, tmin=0.1, frc_t=frc_t)

    # draw velocities
    vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

    # draw their birth radii
    R0 = sample.draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_sampled = len(t)
    mask = (R0 > 0) & (vphi > 0)
    t = t[mask]
    vphi = vphi[mask]
    vr = vr[mask]
    vz = vz[mask]
    JR = JR[mask]
    LZ = LZ[mask]
    JZ = JZ[mask]
    vc0 = vc0[mask]
    Lz0 = Lz0[mask]
    R = R[mask]
    z = z[mask]
    #l = l[mask]
    #b = b[mask]
    #d = d[mask]
    #loc = loc[mask]
    R0 = R0[mask]

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
    p_birth = rm.p_birth(R0, pm_model)*R0/pm_model.rd

    # age distribution
    p_age = rm.SFH_R_1(t, R0, pm_model, tmin=0.1)

    # radial migration
    p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm_model)

    # heating
    Rcirc = rm.R_circ(LZ)
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t, pm_model)
    p_heating = rm.p_heating(JR, k, sigma_r)

    # vertical distribution
    p_z = rm.p_z(Rcirc, R0, t, z)

    # cast all in together
    density_model = p_birth*p_migr*p_heating*p_z*p_age

    # possbibly select redclump stars
    if frc == True:
        f_select_RC = rm.f_RC(t)
        density_model *= f_select_RC

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    if density is None:
        p_position = sample.rho(R, z, pm_sample)
    else:
        p_position = density(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # sample vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)

    # vR 
    p_vR = rm.gauss(0, vr, vel_disp)

    # p_t
    p_t = sample.p_age_IO(t, R, pm_sample, tmin=0.1, frc_t=frc_t)

    # birth radii
    sig = pm_sample.srm*np.sqrt(t/12)
    D = -pm_sample.srm**2/(2*pm_sample.rd*12)
    p_r0 = rm.gauss(R0, R + D*t, sig)

    # cast in  all together
    p_proposal = p_position*p_vphi*p_vR*p_r0*p_t

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model / p_proposal
    max_ratio = np.max(ratio)

    # keep the points with the correct probability
    p_uniform = np.random.rand(len(R))*max_ratio
    mask = p_uniform < ratio

    a = np.sum(mask)/N_sampled


    print(pm_model.t8)
    p_model_list = [p_birth[mask], p_migr[mask], p_heating[mask], p_z[mask]]
    p_proposal_list = [p_position[mask], p_vphi[mask], p_vR[mask], p_r0[mask]]

    if not lbd:
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                  vphi[mask], vr[mask], p_model_list, p_proposal_list]
    if lbd:
        print(d.shape)
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                 vphi[mask], vr[mask], p_model_list, p_proposal_list,\
                                                  l[mask], b[mask], d[mask], loc[mask]]



def sample_mock_mw(pm_sample, pm_model, n,
              showfig=False, lbd=False, density=None, frc=False, frc_t=0):
    """
    2021-01-24

    update from the above to incude apogee-2 fields and be more general
           use all of the usual sampling functions but with the '_DR'
           subscript

    Sample points given sampling parameters and model parameters

    Arguments:
    pm_sample = object of sampling distribution parameters
    pm_model = object of model parameters
    n = number of points in the highest selection fraction field
    fieldstuff = list of [radius, lfield, bfield, selfrac, dmin, dmax]

    Returns:
    Acceptance fraction
    """

    # radii and present heights
    #R = np.random.exponential(scale=10,size=n)
    R = sample.produce_R(pm_sample, n)
    z = np.random.exponential(scale=0.5, size=n)
    z[:int(n/2)] = -z[:int(n/2)]

    nn = len(R)

    # draw ages
    t = sample.sample_age_IO(R, pm_sample, tmin=0.1, frc_t=frc_t)

    # draw velocities
    vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

    # draw their birth radii
    R0 = sample.draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_sampled = len(t)
    mask = (R0 > 0) & (vphi > 0)
    t = t[mask]
    vphi = vphi[mask]
    vr = vr[mask]
    vz = vz[mask]
    JR = JR[mask]
    LZ = LZ[mask]
    JZ = JZ[mask]
    vc0 = vc0[mask]
    Lz0 = Lz0[mask]
    R = R[mask]
    z = z[mask]
    #l = l[mask]
    #b = b[mask]
    #d = d[mask]
    #loc = loc[mask]
    R0 = R0[mask]

    print('positive birth radii:', np.sum(mask), n)

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
    p_birth = rm.p_birth(R0, pm_model)*R0/pm_model.rd

    # age distribution
    p_age = rm.SFH_R_1(t, R0, pm_model, tmin=0.1)

    # radial migration
    p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm_model)

    # heating
    Rcirc = rm.R_circ(LZ)
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t, pm_model)
    p_heating = rm.p_heating(JR, k, sigma_r)

    # vertical distribution
    p_z = rm.p_z(Rcirc, R0, t, z)

    # cast all in together
    density_model = p_birth*p_migr*p_heating*p_z*p_age

    # possbibly select redclump stars
    if frc == True:
        f_select_RC = rm.f_RC(t)
        density_model *= f_select_RC

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    if density is None:
        p_position = sample.rho(R, z, pm_sample)*R
    else:
        p_position = density(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # sample vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)

    # vR 
    p_vR = rm.gauss(0, vr, vel_disp)

    # p_t
    p_t = sample.p_age_IO(t, R, pm_sample, tmin=0.1, frc_t=frc_t)

    # birth radii
    sig = pm_sample.srm*(t/12)**pm_sample.beta_srm
    D = -pm_sample.srm**2/(2*pm_sample.rd*12)
    p_r0 = rm.gauss(R0, R + D*t, sig)

    # cast in  all together
    p_proposal = p_position*p_vphi*p_vR*p_r0*p_t

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model / p_proposal
    max_ratio = np.max(ratio)

    # keep the points with the correct probability
    p_uniform = np.random.rand(len(R))*max_ratio
    mask = p_uniform < ratio

    a = np.sum(mask)/N_sampled


    print(pm_model.t8, a)
    p_model_list = [p_birth[mask], p_migr[mask], p_heating[mask], p_z[mask]]
    p_proposal_list = [p_position[mask], p_vphi[mask], p_vR[mask], p_r0[mask]]

    if not lbd:
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                  vphi[mask], vr[mask], p_model_list, p_proposal_list]
    if lbd:
        print(d.shape)
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                 vphi[mask], vr[mask], p_model_list, p_proposal_list,\
                                                  l[mask], b[mask], d[mask], loc[mask]]




#====================================================================================
# explore parameters for sampling
#====================================================================================
def explore_parameters(p1, p2, p1_name, p2_name, pm_sample, 
                             N, dust=False, showfig=False):
    pm_model = rm.all_parameters()
    pm_model.srm = 4.5*220
#    pm_sample = sample.sampling_parameters()

    # define the acceptance fraction
    a = np.zeros((len(p1), len(p2)))

    # loop over the first parameter
    for i in range(len(p1)):
        pm_sample.__setattr__(p1_name, p1[i])

        # loop over the second parameter
        for j in range(len(p2)):
            pm_sample.__setattr__(p2_name, p2[j])

            # generate the points
#            a[i,j],t,R0,R,z,Lz0,LZ,JR,vphi,vr = \
            a[i,j], t, R0, R, z, \
              Lz0, LZ, JR, vphi, \
              vr, p_model_list, p_proposal_list = \
              sample_points(pm_sample, pm_model, N, dust=dust, showfig=False)

    if showfig == True:
        # plot
        x, y = np.meshgrid(p1, p2, indexing='ij')
        plt.pcolor(x, y, a)
        plt.xlabel(p1_name)
        plt.ylabel(p2_name)
        plt.colorbar(label='acceptance fraction')
        plt.show()

    return a


#=================================================================================
# integrals survey volume
#=================================================================================
def Integral_survey_volume(pm_sample, pm_model, n, dust=False):
    """
    Integrate the model over the survey volume 
    using importance sampling.

    Arguments:
    pm_sample = object of sampling distribution parameters
    pm_model = object of model parameters
    N = number of points in the highest selection fraction field

    Returns:
    Acceptance fraction

    2019: Note: something wrong
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
#    area = np.array(area)
    radius_plate= np.array(radius)
    l_p = np.array(lfield)
    b_p = np.array(bfield)
    selfr = np.array(sel_frac)
    selfrac = selfr

    #==========================================================================
    # draw samples
    #==========================================================================

    # draw positions
    R, z, l, b, d, loc, maxF =  sample.draw_several_pointings_expo(n, l_p, b_p, \
                                         D_min, D_max, pm_sample, radius_plate, \
                              area, selfrac, loc_id, dust=dust, return_maxF=True)

#    plt.figure()
#    plt.scatter(R,z,s=3)
#    plt.show()

    # draw ages
    t = sample.sample_age(len(R), pm_sample)

    # draw velocities
    vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions
    JR, LZ,  JZ = rm.compute_actions(R, z, vr, vphi, vz)

    # draw their birth radii
    R0 = sample.draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_sampled = len(t)
    m = (R0 > 0) & (vphi > 0)
#    t = t[mask]
#    vphi = vphi[mask]
#    vr = vr[mask]
#    vz = vz[mask]
#    JR = JR[mask]
#    LZ = LZ[mask]
#    JZ = JZ[mask]
#    vc0 = vc0[mask]
#    Lz0 = Lz0[mask]
#    R0 = R0[mask]
#    R = R[mask]
#    z = z[mask]

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
    p_birth = rm.p_birth_radius(R0[m], t[m], pm_model)

    # radial migration
    p_migr = rm.p_migration_drift(LZ[m], t[m], Lz0[m], vc0[m], pm_model)
#    p_migr += 1e-500

    # heating
    Rcirc = rm.R_circ(LZ[m])
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t[m], pm_model)
    p_heating = rm.p_heating(JR[m], k, sigma_r) #+ 1e-500

    # vertical distribution
    p_z = np.exp(-np.abs(z[m])/pm_sample.hz)/(2*pm_sample.hz)

    # cast all in together
    density_model = np.zeros(len(R))
    density_model[m] = p_birth*p_migr*p_heating*p_z #+ 1e-500

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    p_position = sample.rho(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # sample vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)

    # vR 
    p_vR = rm.gauss(0, vr, vel_disp)

    # birth radii
    sig = pm_sample.srm*np.sqrt(t/12)
    D = -pm_sample.srm**2/(2*pm_sample.rd*12)
    p_r0 = rm.gauss(R0, R, sig)

    # cast in  all together
    p_proposal = p_position*p_vphi*p_vR*p_r0

    # normalize
#    p_proposal /= np.sum(area*selfrac*maxF)
    p_proposal /=np.sum(maxF)

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model*R0 / p_proposal
#    I = np.sum(ratio)/(n*len(l_p))
#    I = np.sum(ratio)/N_sampled #len(R)
#    I = np.sum(ratio)/N_sampled

    I = np.sum(ratio*area[0])/N_sampled

#    max_ratio = np.max(ratio)

#    p_uniform = np.random.rand(len(R))*max_ratio
#    mask = p_uniform < ratio

#    a = np.sum(mask)/N_sampled
    return I


def Integral_survey_volume_1(pm_sample, pm_model, n, dust=False):
    """
    Compute the survey volume with importance sampling by
    computing the integral in each field and summing over them all

    Arguments
        pm_sample = sampling parameters object
        pm_model = model parameters object
        n = number of points in each field
        dust = boolean: account for dust or not during the sampling

    Return
        the value of the integral

    Note: something wrong
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
    radius_plate= np.array(radius)
    l_p = np.array(lfield)
    b_p = np.array(bfield)
    selfr = np.array(sel_frac)
    selfrac = selfr
    I = np.zeros(len(l_p)) # integral values per field to fill in that loop

    #-------------------------------------------------------------------------
    # loop over all the fields
    #-------------------------------------------------------------------------
    for i in range(len(l_p)):
        print(i)

        #---------------------------------------------------------------------
        # draw samples in field
        #---------------------------------------------------------------------

        # draw positions according to double exponential model
        R, z, l, b, D, f_max = sample.draw_single_pointing_expo(n, l_p[i], 
                                    b_p[i], D_min[i], D_max[i], pm_sample, 
                                      radius_plate[i], locs[i], dust=dust)

        # draw ages
        t = sample.sample_age(len(R), pm_sample)

        # draw velocities (ignore the vertical motion)
        vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
        vz = np.zeros(len(vr))

        # get their actions (ignore vertical structure)
        JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

        # draw their birth radii
        R0 = sample.draw_R0(R, t, pm_sample)

        # get birth angular momentum
        vc0 = rm.calculate_vcirc(R0)
        Lz0 = R0*vc0

        #-----------------------------------------------------------------
        # evaluate model on the samples
        #-----------------------------------------------------------------

        # eliminate negative birth radii
        m = R0 > 0

        # birth radius distribution
        p_birth = rm.p_birth_radius(R0[m], t[m], pm_model)

        # radial migration
        p_migr = rm.p_migration_drift(LZ[m], t[m], Lz0[m], vc0[m], pm_model)

        # heating
        Rcirc = rm.R_circ(LZ[m])
        k = rm.epicycle_frequency(Rcirc)
        sigma_r = rm.vel_disp_r(Rcirc, t[m], pm_model)
        p_heating = rm.p_heating(JR[m], k, sigma_r) #+ 1e-500

        # vertical distribution
        p_z = np.exp(-np.abs(z[m])/pm_sample.hz)/(2*pm_sample.hz)

        # cast all in together
        density_model = np.zeros(len(R))
        density_model[m] = p_birth*p_migr*p_heating*p_z #+ 1e-500

        #-----------------------------------------------------------------
        # evaluate sampling distribution on the samples
        #-----------------------------------------------------------------

        # position distribution
        p_position = sample.rho(R, z, pm_sample)

        # velocity dispersion
        vel_disp = rm.vel_disp_r(R, t, pm_sample)

        # accentuate velocity dispersion to envelope the distribution
        vel_disp *= 1.1

        # sample vphi around the circular velocity at R
        vcirc = rm.calculate_vcirc(R)

        # vphi
        p_vphi = rm.gauss(vcirc, vphi, vel_disp)

        # vR 
        p_vR = rm.gauss(0, vr, vel_disp)

        # birth radii
        sig = pm_sample.srm*np.sqrt(t/12)
        D = -pm_sample.srm**2/(2*pm_sample.rd*12)
        p_r0 = rm.gauss(R0, R, sig)

        # cast in  all together
        p_proposal = p_position*p_vphi*p_vR*p_r0

        # normalize (all is normalzed except for spatial, f_max)
        p_proposal /= f_max

        #-----------------------------------------------------------------
        # take the ratio = gives one integral value per field
        #-----------------------------------------------------------------
        ratio = density_model*R0 / p_proposal
        I[i] = np.sum(ratio)

    # weighted sum over the fields (by area and selection fraction)
    Integral = np.sum(I*selfrac*area)/n

    # return the integral value
    return Integral

def Integral_survey_volume_2(pm_sample, pm_model, n, dust=False):
    """
    2019-01-10

    ----------------------------------------------------------------
    To do: minimize the number of operations in the loop over the
           fields (create a function that does the loop with as
           little things as possible in it and verctorize the rest)
    ----------------------------------------------------------------

    Compute the survey volume with importance sampling by
    computing the integral in each field and summing over them all
    does not distribute the stars in l and b inside the fields
    (does not use the 'cone' routine)

    Arguments
        pm_sample = sampling parameters object
        pm_model = model parameters object
        n = number of points in each field
        dust = boolean: account for dust or not during the sampling

    Return
        the value of the integral

    Note: should be in agreement with Integral_survey_volume_3
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
    radius_plate= np.array(radius)
    area = (1 - np.cos(np.radians(radius_plate)))
    area = np.concatenate(area)
    l_p = np.array(lfield)
    b_p = np.array(bfield)
    selfr = np.array(sel_frac)
    selfrac = selfr
    I = np.zeros(len(l_p)) # integral values per field to fill in that loop

    #-------------------------------------------------------------------------
    # loop over all the fields (takes too much time!!! need something better)
    #-------------------------------------------------------------------------
    for i in range(len(l_p)):

        #---------------------------------------------------------------------
        # draw samples in field (proposal distribution)
        #---------------------------------------------------------------------

        # draw positions according to double exponential model
        R, z, l, b, D, f_max = sample.draw_single_pointing_expo_D(n, l_p[i],
                                      b_p[i], D_min[i], D_max[i], pm_sample,
                                        radius_plate[i], locs[i], dust=dust)

        # f_max is the normalization constant for each field
        # for p( D | l, b, dust)

        # draw ages
        t = sample.sample_age(len(R), pm_sample)

        # draw velocities (ignore the vertical motion)
        vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
        vz = np.zeros(len(vr))

        # get their actions (ignore vertical structure)
        JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

        # draw their birth radii
        R0 = sample.draw_R0(R, t, pm_sample)

        # get birth angular momentum
        vc0 = rm.calculate_vcirc(R0)
        Lz0 = R0*vc0

        #-----------------------------------------------------------------
        # evaluate model on the samples 
        #-----------------------------------------------------------------

        # eliminate negative birth radii
        m = R0 > 0

        # birth radius distribution
        p_birth = rm.p_birth_radius(R0[m], t[m], pm_model)

        # radial migration
        p_migr = rm.p_migration_drift(LZ[m], t[m], Lz0[m], vc0[m], pm_model)

        # heating
        Rcirc = rm.R_circ(LZ[m])
        k = rm.epicycle_frequency(Rcirc)
        sigma_r = rm.vel_disp_r(Rcirc, t[m], pm_model)
        p_heating = rm.p_heating(JR[m], k, sigma_r) #+ 1e-500

        # vertical distribution
        p_z = np.exp(-np.abs(z[m])/pm_sample.hz)/(2*pm_sample.hz)

        # cast all in together (with zeros where R0 < 0)
        density_model = np.zeros(len(R))
        density_model[m] = p_birth*p_migr*p_heating*p_z #+ 1e-500

        #-----------------------------------------------------------------
        # evaluate sampling distribution on the samples
        #-----------------------------------------------------------------

        # position distribution
        p_position = sample.rho(R, z, pm_sample)

        # velocity dispersion
        vel_disp = rm.vel_disp_r(R, t, pm_sample)

        # accentuate velocity dispersion to envelope the distribution
        vel_disp *= 1.1

        # sample vphi around the circular velocity at R
        vcirc = rm.calculate_vcirc(R)

        # vphi
        p_vphi = rm.gauss(vcirc, vphi, vel_disp)

        # vR 
        p_vR = rm.gauss(0, vr, vel_disp)

        # birth radii
        sig = pm_sample.srm*np.sqrt(t/12)
        D = -pm_sample.srm**2/(2*pm_sample.rd*12)
        p_r0 = rm.gauss(R0, R, sig)

        # cast in  all together and normalize with f_max
        p_proposal = p_position*p_vphi*p_vR*p_r0 / f_max

        #-----------------------------------------------------------------
        # take the ratio = gives one integral value per field
        #-----------------------------------------------------------------
        # R0 is a jacobian term that goes with p(R0|t) = R0*Sigma( R0 | t)
        ratio = density_model*R0 / p_proposal
        I[i] = np.sum(ratio)

    # weighted sum over the fields (by area and selection fraction)
    Integral = np.sum(I*selfrac*area)/n
    return Integral


def Integral_survey_volume_3(pm_sample, pm_model, n, dust=False):
    """
    2019-01-10

    ----------------------------------------------------------------
    Faster version of the Integral_Survey_volume_2 routine
    should give the same values
    ----------------------------------------------------------------

    Compute the survey volume with importance sampling by
    computing the integral in each field and summing over them all
    does not distribute the stars in l and b inside the fields
    (does not use the 'cone' routine)

    Arguments
        pm_sample = sampling parameters object
        pm_model = model parameters object
        n = number of points in each field
        dust = boolean: account for dust or not during the sampling

    Return
        the value of the integral

    Note: should be in agreemnet with Integral_survey_volume_2
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters (transform lists to arrays)
    #-------------------------------------------------------------------------
    radius_plate= np.array(radius)
    area = np.array((1 - np.cos(np.radians(radius_plate))))
    l_p = np.array(lfield)
    b_p = np.array(bfield)
    selfr = np.array(sel_frac)  # WTF
    selfrac = selfr             # WTF

    #-------------------------------------------------------------------------
    # Generate points in all the fields
    #-------------------------------------------------------------------------

    # draw positions according to double exponential model
    R, z, l, b, D, ldd, f_max, area_arr, selfrac_arr= \
             sample.draw_several_pointings_expo_D_noreject(n, l_p, b_p, 
                           D_min, D_max, pm_sample, radius_plate, area, 
                            selfrac, locs, dust=dust, return_maxF=True)

    # f_max is the normalization constant for each field
    # for p( D | l, b, dust)

    # draw ages
    t = sample.sample_age(len(R), pm_sample)

    # draw velocities (ignore the vertical motion)
    vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions (ignore vertical structure)
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

    # draw their birth radii
    R0 = sample.draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0

    #-----------------------------------------------------------------
    # evaluate model on the samples 
    #-----------------------------------------------------------------

    # eliminate negative birth radii
    m = R0 > 0

    # birth radius distribution
    p_birth = rm.p_birth_radius(R0[m], t[m], pm_model)

    # radial migration
    p_migr = rm.p_migration_drift(LZ[m], t[m], Lz0[m], vc0[m], pm_model)

    # heating
    Rcirc = rm.R_circ(LZ[m])
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t[m], pm_model)
    p_heating = rm.p_heating(JR[m], k, sigma_r)

    # vertical distribution
    p_z = np.exp(-np.abs(z[m])/pm_sample.hz)/(2*pm_sample.hz)

    # cast all in together (with zeros where R0 < 0)
    density_model = np.zeros(len(R))
    density_model[m] = p_birth*p_migr*p_heating*p_z

    #-----------------------------------------------------------------
    # evaluate sampling distribution on the samples
    #-----------------------------------------------------------------

    # position distribution
    p_position = sample.rho(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # sample vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi and vr
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)
    p_vR = rm.gauss(0, vr, vel_disp)

    # birth radii
    sig = pm_sample.srm*np.sqrt(t/12)
    D = -pm_sample.srm**2/(2*pm_sample.rd*12)
    p_r0 = rm.gauss(R0, R, sig)

    # cast in  all together and normalize p(D|field) with f_max
    p_proposal = p_position*p_vphi*p_vR*p_r0/f_max

    #-----------------------------------------------------------------
    # take the ratio = gives one integral value per field
    #-----------------------------------------------------------------
    # R0 is a jacobian term that goes with p(R0|t) = R0*Sigma( R0 | t)
    ratio = density_model*R0 / p_proposal
    Integral = np.sum(ratio*selfrac_arr*area_arr)/n
    return Integral


def Integral_survey_volume_4(pm_sample, pm_model, n, dust=False):
    """
    2019-01-14

    ----------------------------------------------------------------
    Relative number of points in each field consistent with:
        -- Galactic density model
        -- Selection fraction model
        -- Relative area of each field plate
    ----------------------------------------------------------------

    Compute the survey volume with importance sampling
    does (not? check) distribute the stars in l and b inside the fields
    (does (not? check) use the 'cone' routine)

    Arguments
        pm_sample = sampling parameters object
        pm_model = model parameters object
        n = number of points in each field
        dust = boolean: account for dust or not during the sampling

    Return
        the value of the integral

    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters (transform lists to arrays)
    #-------------------------------------------------------------------------
    radius_plate= np.array(radius)
#    area = np.array((1 - np.cos(np.radians(radius_plate))))
    l_p = np.array(lfield)
    b_p = np.array(bfield)
    selfr = np.array(sel_frac)  # WTF
    selfrac = selfr             # WTF

    #-------------------------------------------------------------------------
    # Generate points in all the fields
    #-------------------------------------------------------------------------

    # draw positions
    R, z, l, b, d, loc, maxF, Nkeep = \
                        sample.draw_several_pointings_expo(n, l_p, b_p, \
                  D_min, D_max, pm_sample, radius_plate, area, selfrac, \
              loc_id, dust=dust, return_maxF=True, return_Nkeep = True)

    # f_max is the normalization constant for each field
    # for p( D | l, b, dust)

    # draw ages
    t = sample.sample_age(len(R), pm_sample)

    # draw velocities (ignore the vertical motion)
    vphi, vr = sample.draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions (ignore vertical structure)
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

    # draw their birth radii
    R0 = sample.draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0

    #-----------------------------------------------------------------
    # evaluate model on the samples 
    #-----------------------------------------------------------------

    # eliminate negative birth radii
    m = R0 > 0

    # birth radius distribution
    p_birth = rm.p_birth_radius(R0[m], t[m], pm_model)

    # radial migration
    p_migr = rm.p_migration_drift(LZ[m], t[m], Lz0[m], vc0[m], pm_model)

    # heating
    Rcirc = rm.R_circ(LZ[m])
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t[m], pm_model)
    p_heating = rm.p_heating(JR[m], k, sigma_r)

    # vertical distribution
    p_z = np.exp(-np.abs(z[m])/pm_sample.hz)/(2*pm_sample.hz)

    # cast all in together (with zeros where R0 < 0)
    density_model = np.zeros(len(R))
    density_model[m] = p_birth*p_migr*p_heating*p_z

    #-----------------------------------------------------------------
    # evaluate sampling distribution on the samples
    #-----------------------------------------------------------------

    # position distribution
    p_position = sample.rho(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # sample vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi and vr
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)
    p_vR = rm.gauss(0, vr, vel_disp)

    # birth radii
    sig = pm_sample.srm*np.sqrt(t/12)
    D = -pm_sample.srm**2/(2*pm_sample.rd*12)
    p_r0 = rm.gauss(R0, R, sig)

    # cast in  all together
    p_proposal = p_position*p_vphi*p_vR*p_r0

    #-----------------------------------------------------------------
    # take the ratio = gives one integral value per field
    #-----------------------------------------------------------------
    # R0 is a jacobian term that goes with p(R0|t) = R0*Sigma( R0 | t)
    ratio = density_model*R0 / p_proposal

    # total number of stars and normalization constant
    N_tot = len(R)
    print(N_tot)


    # we want fmax = np.sum(maxF*selfrac*area)
    fmax = np.sum(maxF*selfrac*np.array(area))  
    Integral = np.sum(ratio)/N_tot*fmax
    return Integral

def Sample_survey_volume_mcmc(pm_sample, n, mcmc_args, dust=False):
    """
    2019-01-15

    ----------------------------------------------------------------
    Relative number of points in each field consistent with:
        -- Galactic density model
        -- Selection fraction model
        -- Relative area of each field plate
    ----------------------------------------------------------------

    Compute the survey volume with MCMC directly from the model
    does (not? check) distribute the stars in l and b inside the fields
    (does (not? check) use the 'cone' routine)

    Arguments
        pm_sample = model parameters object used for sampling
        n = number of points in each field
        mcmc_args = array of mcmc parameters:
            ndim = number of dimensions (data)
            ncpu = for parallelization
            niter =  number of mcmc iterations
            nburn = number of burning steps
        dust = boolean: account for dust or not during the sampling

    Return
        the samples in the fields according to the distribution

    """
    # initial conditions

    # mcmc parameters
    ndim, ncpu, niter, nburn = mcmc_args

    # initialize the sampler

    # run the mcmc

    # get the samples
    
    return 1

def Integral_survey_volume_MCMC(samples, pm_samples, pm_model):
    """
    2019-01-15

    Compute the survey volume from points sampled by mcmc from pm_samples. 
    Reweight the integral by the model evaluated at pm_modl.

    Arguments
        samples = data space: l, b, D, R0, t, vR, vphi
        pm_samples = model parameters object used to produce the samples
        pm_model = model parameters object we want to compute
                   the survey volume for.

    Returns
        the value of the integral
    """
    return 1



##############################################################################
# simpified version of the sampling
##############################################################################

def sample_points_simple(pm_sample, pm_model, n, dust=False, showfig=True):
    """
    Sample points given sampling parameters and model parameters

    Arguments:
    pm_sample = object of sampling distribution parameters
    pm_model = object of model parameters
    N = number of points in the highest selection fraction field

    Returns:
    Acceptance fraction
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
    area = 0
    radius_plate= np.array(radius)
    l_p = np.array(lfield)
    b_p = np.array(bfield)
    selfr = np.array(sel_frac)
    selfrac = selfr

    #==========================================================================
    # draw samples
    #==========================================================================

    # draw positions
    R, z, l, b, d, loc =  sample.draw_several_pointings_expo(n, l_p, b_p, \
         D_min, D_max, pm_sample, radius_plate, area, selfrac, loc_id, dust)

    # draw ages
    t = sample.sample_age(len(R), pm_sample)

    # draw their birth radii
    R0 = sample.draw_R0(R, t, pm_sample)

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_sampled = len(t)
    mask = (R0 > 0)
    t = t[mask]
    R0 = R0[mask]
    R = R[mask]
    z = z[mask]

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
    p_birth = rm.p_birth_radius(R0, t, pm_model)

    # radial migration
#    LZ = R*220
#    Lz0 = R0*220
#    vc0 = 220
#    p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm_model)
    p_migr = rm.p_migration_radius(R, t, R0, pm_model)
#    p_migr += 1e-500

    # vertical distribution
#    p_z = np.exp(-np.abs(z)/pm_sample.hz)/(2*pm_sample.hz)
    hz = np.zeros(len(z))
    hz[t < 4] = 0.5  #pm_sample.hz
    hz[t >= 4] = 0.5 #pm_sample.hz*3
    p_z = np.exp(-np.abs(z)/hz)/(2*hz)

    # cast all in together
    density_model = p_birth*p_migr*p_z #+ 1e-400

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    p_position = sample.rho(R, z, pm_sample)

    # birth radii
    sig = pm_sample.srm*np.sqrt(t/12)
    D = -pm_sample.srm**2/(2*pm_sample.rd*12)*pm_sample.c4
    p_r0 = rm.gauss(R0, R + D*t, sig)

    # cast in  all together
    p_proposal = p_position*p_r0

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model*R / p_proposal
    max_ratio = np.max(ratio)

    # keep the points with the correct probability
    p_uniform = np.random.rand(len(R))*max_ratio
    mask = p_uniform < ratio

    a = np.sum(mask)/N_sampled

    if showfig == True:
        print('Acceptrance fraction a = ', a)
        print('Number of points = ', np.sum(mask))
#        plt.scatter(LZ[mask], np.sqrt(JR[mask]), s=2, c=t[mask], alpha=0.1)
#        plt.xlabel('Lz')
#        plt.ylabel('Jr')
#        plt.show()
        plt.hist(R, density=1, alpha=0.5, bins=50, label='proposed')
        plt.hist(R[mask], density=1, bins=50, alpha=0.5, label='accepted')
        plt.legend()
        plt.xlabel('R [kpc]')
        plt.show()

    p_model_list = [p_birth[mask], p_migr[mask], p_z[mask]]
    p_proposal_list = [p_position[mask], p_r0[mask]]

#    return a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], 
#                                          p_model_list, p_proposal_list
    return a, t[mask], R0[mask], R[mask], z[mask], \
                     p_model_list, p_proposal_list

###################################################################################
# even simpler sampling
###################################################################################
def sample_points_simpler(pm_sample, pm_model, n, dust=True, showfig=True):
    """
    Sample points given sampling parameters and model parameters

    Arguments:
    pm_sample = object of sampling distribution parameters
    pm_model = object of model parameters
    N = number of points in the highest selection fraction field

    Returns:
    Acceptance fraction
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
    area = 0
    radius_plate= np.array(radius)
    l_p = np.array(lfield)
    b_p = np.array(bfield)
    selfr = np.array(sel_frac)
    selfrac = selfr

    #==========================================================================
    # draw samples
    #==========================================================================

    # draw positions
    R, z, l, b, d, loc =  sample.draw_several_pointings_expo(n, l_p, b_p, \
         D_min, D_max, pm_sample, radius_plate, area, selfrac, loc_id, dust=dust)

    # draw ages
    t = sample.sample_age(len(R), pm_sample)

    # draw their birth radii
#    R0 = sample.draw_R0(R, t, pm_sample)
    R0 = np.random.exponential(size=len(R), scale=pm_sample.rd)

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_sampled = len(t)
    mask = (R0 > 0)
    t = t[mask]
    R0 = R0[mask]
    R = R[mask]
    z = z[mask]
    l = l[mask]
    b = b[mask]
    loc = loc[mask]
    d = d[mask]

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
#    p_birth = rm.p_birth_radius(R0, t, pm_model)
    p_birth = np.exp(-R0/pm_model.rd)/pm_model.rd

    # radial migration
#    LZ = R*220
#    Lz0 = R0*220
#    vc0 = 220
#    p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm_model)
#    p_migr = rm.p_migration_radius(R, t, R0, pm_model)
#    p_migr += 1e-500
    p_migr = np.exp(-R/pm_model.rd)/pm_model.rd

    # vertical distribution
#    p_z = np.exp(-np.abs(z)/pm_sample.hz)/(2*pm_sample.hz)
    hz = np.zeros(len(z))
    hz[t < 4] = 0.5  #pm_sample.hz
    hz[t >= 4] = 0.5 #pm_sample.hz*3
    p_z = np.exp(-np.abs(z)/hz)/(2*hz)

    # cast all in together
#    density_model = p_birth*p_z*p_migr #+ 1e-400
    density_model = p_migr*p_z

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    p_position = sample.rho(R, z, pm_sample)

    # birth radii
#    sig = pm_sample.srm*np.sqrt(t/12)
#    D = -pm_sample.srm**2/(2*pm_sample.rd*12)*pm_sample.c4
#    p_r0 = rm.gauss(R0, R + D*t, sig)
    p_r0 = np.exp(-R0/pm_sample.rd)/pm_sample.rd

    # cast in  all together
#    p_proposal = p_position*p_r0
    p_proposal = p_position

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model / p_proposal
    max_ratio = np.max(ratio)

    # keep the points with the correct probability
    p_uniform = np.random.rand(len(R))*max_ratio
    mask = p_uniform < ratio

    a = np.sum(mask)/N_sampled

    if showfig == True:
        print('Acceptrance fraction a = ', a)
        print('Number of points = ', np.sum(mask))
#        plt.scatter(LZ[mask], np.sqrt(JR[mask]), s=2, c=t[mask], alpha=0.1)
#        plt.xlabel('Lz')
#        plt.ylabel('Jr')
#        plt.show()
        plt.hist(R, density=1, alpha=0.5, bins=50, label='proposed')
        plt.hist(R[mask], density=1, bins=50, alpha=0.5, label='accepted')
        plt.legend()
        plt.xlabel('R [kpc]')
        plt.show()

#    p_model_list = [p_birth[mask], p_migr[mask], p_z[mask]]
#    p_proposal_list = [p_position[mask], p_r0[mask]]

    return a, t[mask], R0[mask], R[mask], z[mask], 1, 1 

#Lz0[mask], LZ[mask], p_model_list, p_proposal_list
#    return a, t[mask], R0[mask], R[mask], z[mask], 
#           l[mask], b[mask], d[mask], loc[mask] 
#           p_model_list, p_proposal_list


def p_model(D, R0, t, vphi, vr, lfield, bfield, 
           selfrac, area, lid, pm, dust=False):
    """
    Evaluate the model pdf in data space

    Arguments
    D = distance to Sun
    R0 = birth galactocentric radius
    t = age
    vphi = azimuthal velocity
    vr = radial velocity
    lfield = longitude field
    bfield = latitude field
    selfrac = selection fraction field
    area = area plate field
    pm = model parameters

    Returns
    model pdf
    """
    # age
    c1 = norm_c1(tmin, tmax, pm)
    p_age = rm.p_age(t, c1, pm)

    # birth radius
    p_birth = rm.p_birth_radius(R0, t, pm)

    # birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0

    # angular momentum diffusion
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)
    p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm)

    # heating
    Rcirc = rm.R_circ(LZ)
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t, pm)
    p_heating = rm.p_heating(JR, k, sigma_r)

    # jacobian terms 
    Jac_D = D*D
    Jac_vphi = R

    # dust
    if dust == True:
        p_dust = sample.fraction_area(D, lid)
    else:
        p_dust = 1.

    # cast in all together
    p = p_age*p_birth*p_migr*p_heating\
                *Jac_D*Jac_vphi*p_dust

    return p



if __name__=='__main__':
    rd = 2.5
    srm = 6.
    pm_sample = sample.sampling_parameters(rd=2.5, srm=6)
    pm_model = rm.all_parameters(srm=4.5*220)
    a = sample_points(pm_sample, pm_model, 100)

    #rd = np.linspace(2, 6, 20)
    #srm = np.linspace(3, 12, 50)
    #explore_parameters(rd, srm, 'rd', 'srm', 1000)








