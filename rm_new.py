"""
rm_new
-------
Main functions in the radial migration model
"""

# Radial migration model with actions
# created 03/05/2018
# modified 13/05/2019

#-----------------------------------------------------------
# import packages
import numpy as np
import matplotlib.pyplot as plt
import scipy.special as scp
import scipy.integrate as integrate
import time
from scipy.optimize import minimize

# galpy stuff
from galpy.potential import MWPotential2014 as pot
from galpy import potential
from galpy.potential import verticalfreq, epifreq, omegac
from galpy.util import bovy_coords
from galpy.potential import plotRotcurve, vcirc
from galpy.actionAngle import actionAngleStaeckel
from astropy.coordinates import SkyCoord                       # High-level coordinates
from astropy.coordinates import ICRS, Galactic, FK4, FK5       # Low-level frames
from astropy.coordinates import Angle, Latitude, Longitude     # Angles


#-----------------------------------------------------------


# global constants
tau_max = 12
tau_min = 0.23
pmin = 1e-50

# galpy scale units:
_REFR0 = 8.   #[kpc]  --> galpy length unit
_REFV0 = 220. #[km/s] --> galpy velocity unit


#==================================================================================================
# model parameters class
#==================================================================================================

#--------------------------------------------------------------------------------------------------
# enrichment parameters
#--------------------------------------------------------------------------------------------------
class enrichment_parameters(object):
    """
    Class instance for enrichment parameters to pass to the enrichment functions.

    Sorry for being disorganized (was the first time I wrote a clas...)
    """
    def __init__(self, tsfr=None, gamma=None, t_el=None, Rs=None, grad=None, Fm=None, 
                 sigma_vr0=None, t1=None, t_T=None, beta=None, c4=None, 
                 grad_in = None, Rbreak=None, Femax=None, eta_a=None, eta_b=None, x=None):
        """
        Initialize the model parameters
        """
        if gamma is not None:
            self.gamma = gamma
        else:
            self.gamma = 0.3		# gamma for enrichment power law
        if t_el is not None:
            self.t_el = t_el
        else:
            self.t_el = 4		# t_el for enrichment exponential 
        if Rs is not None:
            self.Rs = Rs
        else:
            self.Rs = 8			# radius of solar abundance
        if grad is not None:
            self.grad = grad
        else:
            self.grad = -0.07		# metallicity gradient
        if Fm is not None:
            self.Fm = Fm
        else:
            self.Fm = -1                # min abundance at 0 kpc 12 Gyr ago
        if grad_in is not None: self.grad_in = grad_in
        else: self.grad_in = -0.04
        if Rbreak is not None: self.Rbreak = Rbreak
        else: self.Rbreak = 3
        if Femax is not None: self.Femax = Femax
        else: self.Femax = 0.7
        if eta_a is not None: self.eta_a = eta_a
        else: self.eta_a = 0
        if eta_b is not None: self.eta_b = eta_b
        else: self.eta_b = 2.5
        if x is not None: self.x = x
        else: self.x = 0.7
        if tsfr is not None: self.tsfr = tsfr
        else: self.tsfr = 3


    def enrichment_array(self, enrichment_type):
        """
        returns a numpy array of enrichment parameters
        depending on what enrichment prescription is used

        Parameters
        ----------
        enrichment_type: string,
            'gamma' or 'expo'
            gamma = the gamma power law (Frankel+19,20) chemical enrichment model
            expo = the exponential enrichment model (Sanders+15)
        
        Returns
        --------
        Rs: float
            Radius of solar metallicity
        gamma or tel: float
            Time exponent or exponential time scale
        grad: metallicity gradient
        
        """ 
        if enrichment_type == 'gamma':
            return np.array([self.Rs, self.gamma, self.grad])
        elif enrichment_type == 'expo':
            return np.array([self.Rs, self.t_el, self.grad])
        else:
            print("I expected 'gamma' or 'expo' as an argument")


    def update(self, pm_names, pm_val):
        """
        Update the elements of the parameter object

        Parameters
        ------------
        pm: parameter class instance
            model parameters
        pm_names = list of strings
            names of the model parameters ['tsfr', 'arexp', ...]
        pm_val: list of floats
             values of the model parameters [8, 0.5, ...]

        Returns
        --------
        None
        updated pm
        """
        for i in range(len(pm_names)):
            setattr(self, pm_names[i], pm_val[i])

#----------------------------------------------------------------------------------------------------
# define all model parameters in here
#----------------------------------------------------------------------------------------------------
class all_parameters(object):
    """ 
    Class for the parameters to pass to the full model
    """
    def __init__(self, elements=None, pm_elements=None, srm=None, tsfr=None, arexp=None, 
                 feold=None, sfeold=None, rd=None, sigma_vr0=None, t1=None, t_T=None,
                 beta=None, c4=None, Rd_heating=None, a=None, x=None, eps=None, t_old=None,
                 hold=None, beta_srm=None, a0=None, a1=None, b0=None, b1=None, c0=None,
                 c1=None, c2=None, c3=None):
        """
        initialize the parameters
        """
        #---------------------------------------------------------
        # enrichment parameters
        #---------------------------------------------------------
        # self.enrich is an object containing N 
        # enrichment objects, one object per element
        i = 0          
        if elements is not None:
            for xx in elements:
                print(xx)
                if pm_elements is not None:
                    self.__setattr__('enrich_'+xx, pm_elements[i])
                    i += 1
                else:
                    self.__setattr__('enrich_'+xx, enrichment_parameters())
            
        #---------------------------------------------------------
        # other parameters
        #---------------------------------------------------------

        if srm is not None: self.srm = srm                     # radial migration
        else: self.srm = 3.5*220
        if tsfr is not None: self.tsfr = tsfr                  # star formation time-scale
        else: self.tsfr = 8
        if arexp is not None: self.arexp = arexp               # inside-out growth
        else: self.arexp = 0.5
        if feold is not None: self.feold = feold               # old metallcity mean
        else: self.feold = 0.
        if sfeold is not None: self.sdeold = sfeold            # old metallicity std
        else: self.sfeold = 0.1
        if rd is not None: self.rd = rd                        # scale lenght stars at birth
        else: self.rd = 3.
        if sigma_vr0 is not None: self.sigma_vr0 = sigma_vr0
        else: self.sigma_vr0 = 30.                             # velocity dispersion at 8 kpc
        if t1 is not None: self.t1 = t1
        else: self.t1 = 0.11
        if t_T is not None: self.t_T = t_T                     # age transition thin-thick disk
        else: self.t_T = 8.
        if beta is not None: self.beta = beta                  # radial heating power law with time
        else: self.beta = 0.33
        if c4 is not None: self.c4 = c4                        # conservation of angular momentum?
        else: self.c4 = 0.5
        if Rd_heating is not None: self.Rd_heating = Rd_heating  # decay heating lengthscale
        else: self.Rd_heating = 6        
        self.R8 = 8
        if a is not None:  self.a = a                          # Adapt YST model scale height
        else: self.a = 1
        if x is not None: self.x = x                           # inside-out growth
        else: self.x = 0.7
        if eps is not None: self.eps = eps                     # outlier fraction (old stars)
        else: self.eps = 0.05
        if t_old is not None: self.t_old = t_old               # transition young/old
        else: self.t_old = 10
        self.told = self.t_old
        self.t8 = self.t_old
        if hold is not None: self.hold = hold                  # scale-height old stars
        else: self.hold = 10
        if beta_srm is not None: self.beta_srm = beta_srm      # radial migration exponent
        else: self.beta_srm = 0.5
        # vertical heating parameters (Taken from the model of Ting+18)
        if a0 is not None: self.a0 = a0
        else: self.a0 = 1.05
        if a1 is not None: self.a1 = a1
        else: self.a1 = 0.55*1e-1
        if b0 is not None: self.b0 = b0
        else: self.b0 = 1.79
        if b1 is not None: self.b1 = b1
        else: self.b1= 0.5*1e-1
        if c0 is not None: self.c0 = c0
        else: self.c0 = 0.91
        if c1 is not None: self.c1 = c1
        else: self.c1 = 1.83*1e-1
        if c2 is not None: self.c2 = c2
        else: self.c2 = 8.7e-2
        if c3 is not None: self.c3 = c3
        else: self.c3 = 14e-3

    def parameter_array(self):
        """
        Returns a numpy array of parameters (except for enrichment)
        """
        pm_rest = np.array([self.tsfr, self.arexp, self.srm,\
                self. feold, self.rd])

        return pm_rest

    def update(self, pm_names, pm_val):
        """
        Update the elements of the parameter object

        Parameters
        ----------
        pm: parameter class instance
            model parameters
        pm_names: list of strings
               ['tsfr', 'arexp', ...]
        pm_val: list of floats
              parameters values [8, 0.5, ...]

        Returns
        --------
        None
        updated pm
        """
        for i in range(len(pm_names)):
            # split name of pm_names
            pm_name = pm_names[i].split('.')
            if len(pm_name) == 1:
                setattr(self, pm_name[0], pm_val[i])
            if len(pm_name) == 2:
                pmenrich = getattr(self, pm_name[0])
                setattr(pmenrich, pm_name[1], pm_val[i])


        

#==================================================================================================
# useful functions
#==================================================================================================
# gaussian function
def gauss(x, mu, sigma):
    """
    Gaussian(x, mu, sigma)

    Parameters
    ----------
    x: array
       variable
    mu: array
       mean of the Gaussian distribution
    sigma: array
       std of the Gaussian distribution

    Returns
    -------
        array
        Gauss(x, mu, sigma)
    """
    return np.exp(-(x - mu)*(x - mu)/(2*sigma*sigma))/(np.sqrt(2*np.pi)*sigma)

def integrate_gauss(t, t_min, t_max, err):
    """
    Integrate a Gaussian(t, mu=0, err) from tmin to tmax
    Used to marginalize over gaussian errors between two limiting values

    Parameters
    ----------
    t: array
       variable to integrate over
    t_min: float
       lower bound of the integral
    t_max: float
       upper bound of the integral
    err: array
        standard deviation of the Gaussian
    """
    I = 0.5*(scp.erf((t - t_min)/np.sqrt(2*err*err)) - \
            (scp.erf((t - t_max)/np.sqrt(2*err*err))))
    return I

# integral of gaussian function (used in normalization age errors)
def integrate_gauss_log(t, t_min, t_max, err_a):
    """
    Integrate a Gaussian in log space: used to marginalize over errors
    that are Gaussian in Log space

    Parameters
    ----------
    t: array
       variable to integrate over (linear)
    t_min: float
       lower bound of the integral (linear)
    t_max: float
       upper bound of the integral (linear)
    err_a: array
        standard deviation of the Gaussian in log
    """
    a_min, a_max, a  = np.log(t_min), np.log(t_max), np.log(t)
    I = 0.5*(scp.erf((a - a_min)/np.sqrt(2*err_a*err_a)) - \
            (scp.erf((a - a_max)/np.sqrt(2*err_a*err_a))))
    return I

def sech2(x):
    """
    sech squared distribution

    Parameters
    ----------
    x: float, int or array

    Returns
    -------
    sech2(x), same format as x

    
    2019-01-19
    """
    return 1./np.cosh(x)**2


#===================================================================================================
# Functionnal forms for the model
#===================================================================================================
#-----------------------------------------------------------------------------------------
# Expected number of RC stars per unit stellar mass given their age  Bovy (2014)
#-----------------------------------------------------------------------------------------
def f_RC(tau): # tau = stellar age [Gyr]
        """
        Proba to be a RC star given stellar age (Bovy 2014)

        Parameters
        -----------
        tau: float or array
            Age in Gyr

        Returns 
        -------
        p(RC|tau)  (type and size of tau)
        """
        a = np.log10(tau)
        # to do depends on the type of variable tau: array or float? 
        if isinstance(tau, np.ndarray) or isinstance(tau, list): # array
                b = np.zeros(a.shape)
                f = a*0
                mask0 = a<np.log10(tau_min)
                f[mask0]=np.log(pmin)
                mask1 = (a< 0.1)
  #              a[mask1] = -0.1
                b = b + 0.1 #-0.1*np.ones(len(a))
                fb = -1.6314 + 3.8230*b[mask1] + 2.2133*b[mask1]*b[mask1] \
                                        - 35.7414*b[mask1]*b[mask1]*b[mask1]
                f[mask1] = fb + (a[mask1] - b[mask1])\
                        *( 3.8230 + 2*2.2133*b[mask1] - 3*35.7414*b[mask1]*b[mask1])
                mask2 = (a > 0.1) & (a<=0.23)
                f[mask2] = -1.6314 + 3.8230*a[mask2] + 2.2133*a[mask2]*a[mask2] \
                                                - 35.7414*a[mask2]*a[mask2]*a[mask2]
                mask3 = a>0.23
                f[mask3] = -1.0666 + 1.8664*a[mask3] - 9.0617*a[mask3]*a[mask3] \
                                                + 4.5860*a[mask3]*a[mask3]*a[mask3]
                f[f==0]=-50
        elif isinstance(tau,float) or isinstance (tau, int):  # float
                f = -53
                if a < np.log10(tau_min):
                        f = np.log(pmin)
                elif a < -0.1:
 #                       a = -0.1
                        b = -0.1
                        fb = -1.6314 + 3.8230*b + 2.2133*b*b - 35.7414*b*b*b
                        f = fb + (a - b)*( 3.8230 + 2*2.2133*b - 3*35.7414*b*b)
                if (a < 0.23) & (a >= -0.1):
                        f = -1.6314 + 3.8230*a + 2.2133*a*a - 35.7414*a*a*a
                elif a > 0.23:
                        f = -1.0666 + 1.8664*a - 9.0617*a*a + 4.5860*a*a*a
        else :
                print('in f_RC, tau is not float or int or np.ndarray, check what tau is')
        f1 = np.exp(f)
        return f1

#-------------------------------------------------------------------------------------------------
# Star formation history (SFH)
#-------------------------------------------------------------------------------------------------
def SFH(t, param):
    """
    (not normalized) star formation history

    Parameters
    ----------
    t: float or array
        age (Gyr) 
    param: model parameters class instance
        model parameters. Need param.tsfr 

    Returns
    -------
    SFH(tau) (same type and dimensions as t)
    """
    return np.exp(-(tau_max - t)/param.tsfr)

def SFH_R_1(t, R, pm, tmin=tau_min):
    """
    (not normalized) star formation history as a functoin of Galactocentric radius.

    Parameters
    -----------
    t: array or float
       age (Gyr)
    R: array or float
       birth radius (kpc)
    pm: model_parameters class instance

    Returns
    -------
        SFH(t, R)


    Notes
    ------

    function defined and used in inside-out growth paper
          sub_proj_sel_z/model_..._class.py


    History:

    2019-05-13 frankel
    """
    tsfr2 = pm.tsfr + 0
    a = pm.x

    #print(tsfr2, R, alpha)
    l = 1/tsfr2*R/8*a
    y = np.exp(-t*(l - 1/tsfr2) - tau_max/tsfr2)
    c = -1./(l - 1./tsfr2)*(np.exp(-pm.t8*(l - 1/tsfr2) - tau_max/tsfr2)\
                           -np.exp(-tmin*(l - 1/tsfr2) - tau_max/tsfr2))

    if isinstance(t, np.ndarray):
        y[t >  pm.t8] = 0
    else:
        if t > pm.t8: y = 0
    return y/c

def SFH_expo(t, R, pm, tmin=tau_min):
    """
    Star formation history (exponential)
    as modeled in Weinberg and going with the Weinberg prescription

    Parameters
    ----------
    t: array
        lookback time [Gyr]
    R: array
        birth radius [kpc]

    Returns
    -------
    SFH
    """
    t_sfr = pm.tsfr + pm.x*R/8
    y = np.exp(-(11-t)/t_sfr)
    norm = t_sfr*(1-np.exp(-tmax/t_sfr))
    return y/norm

#-------------------------------------------------------------------------------------------------
# Age distribution of red clump stars
# Bovy (2014)
#-------------------------------------------------------------------------------------------------
def p_age(tau, c1, param):
    """
    Age distribution of RC stars

    Parameters
    -----------
    tau: array
        age (Gyr) (array or float)
    c1: array
        normalization constant for p_age between tau_min and tau_max
    param: model parameters class instance

    Returns
    -------
    the pdf of tau
    """
    return c1*SFH(tau, param)*f_RC(tau)

def p_age_R_1(t, R, c1, pm, frc=True):
    """
    Age distributions at different birth radii (normalized by c1)

    Arguments
        t = age Gyr
        R = birth radius kpc
        c1 = normalization constant
        pm = model parameters object
        frc = boolean: select RC stars only = True, not = False

    Return:
        pdf of age at given galactocentric radii

    2019-05-13
    """
    y = c1*SFH_R_1(t, R, pm)
    if frc == True: y *= f_RC(t)
    y[t > pm.t8] = 0
    return y

# normalization factor for age distribution
def norm_c1(tmin, tmax, param):
    """
    calculates the normalization integral for f_RC(tau)

    arguments
    tmin, tmax = integration limits
    model = model parameters 

    returns 1/integral
    """
    I = integrate.quad(lambda tau: p_age(tau, 1,  param),tmin,tmax)[0]
    return 1/I

def norm_c1_R_1(R0, tmin, tmax, pm, frc=True):
    """
    Calculates the normalization integral for the SFH_R_1
    The SFH is normalized at each birth radius

    Arguments:
        birth radius R0 kpc (1D array)
        tmin, tmax = min and max age integration limits (floats)
        pm = model parameters object
        frc = boolean: True = select RC, False = no selection

    Return:
        An array of constants of size R0

    2019-05-13
    """
    tvec = np.linspace(tmin, tmax, 60)
    tvec.shape += (1,)
    tshaped, R0shaped = np.meshgrid(tvec, R0, indexing='ij')
    p = p_age_R_1(tshaped, R0shaped, 1, pm, frc=frc)
    I = np.trapz(p, tvec, axis=0)
    return 1/I

def norm_c1_R_2D_1(R0, tmin, tmax, pm, frc=True):
    """
    Same as norm_c1_R but R0 is a 2D array

    2019-05-13
    """
    tvec = np.linspace(tmin, tmax, 60)
    tvec.shape += (1,)

    # increase the dimension
    tvec_ND = np.ones((R0.shape[0], R0.shape[1], len(tvec)))*tvec.T
    R0shaped = (R0.T*np.ones((R0.shape[0], R0.shape[1], len(tvec))).T).T

    # p age and sum
    p = p_age_R_1(tvec_ND, R0shaped, 1, pm, frc=frc)
    I = np.trapz(p, tvec_ND, axis=2)
    return 1/I

def page_old(t, tmean, tmin, std=2):
    """
    Age distribution of old stars
    Gaussian centered on tmean and 2 Gyr std
    truncated at tmin (boundary young/old)

    2019-05-13
    """
    y = gauss(t, tmean, std)
    y [(t > tmin)] = 0
    c = integrate_gauss(tmean, tmin, 12, std)
    return y/c



#-----------------------------------------------------------------------------------------------
# radial birth profile
#-----------------------------------------------------------------------------------------------
# Scale length, exponential decay, modeling inside out growth
def R_exp_func(tau, param):
    """
    Inside-out feature: radial scale length of the star forming disk that can
    increase with time.

    arguments:
    tau: lookback time (or age of star formed) (Gyr)
    model parameters (look at argument 1)

    returns:
    the radial scale length (kpc) 3*(1-model[1]*tau/tau_max)
    """        
    return param.rd*np.sqrt(1 - param.arexp*tau/tau_max)   # [kpc]

# Radial birth profile
def p_birth_radius(R_0, t, param):
    """
    Radial birth profile

    arguments:
    R_0 = birth radius [kpc] array or float
    tau = age or lookback time [Gyr] array or float
    model = array of model parameters
    """
    if isinstance(R_0, float):
            p = 1e-50
            if R_0 >= 0:
                    R_exp = R_exp_func(t, param)
                    c2 = 1./R_exp
                    p = c2*np.exp(-R_0/R_exp)
            if (p <= 0):
                    #print('radial birth profile neg ', p)
                    p = 1e-100
    elif isinstance(R_0, np.ndarray):
            p = np.zeros(R_0.shape) + 1e-100
            R_exp = np.zeros(R_0.shape)
            R_exp[R_0 >= 0] = R_exp_func(t[R_0 >= 0], param)
            c2 = 1./R_exp[R_0 >= 0]
            p[R_0 >= 0] = c2*np.exp(-R_0[R_0 >= 0]/R_exp[R_0 >= 0])
    return p

def p_birth(R0, pm):
    """
    Overall radial birth profile (exponential)

    Parameters
    -----------
    R0: array,
        birth radius (kpc)
    pm: model parameters class instance

    Returns
    --------
    p(R0 | pm)

    2019-05-13
    """
    return np.exp(-R0/pm.rd)/pm.rd
    

#====================================================================================================
#====================================================================================================
# Enrichment prescription
#====================================================================================================
class element(object):
    """
    Enrichment prescriptions for single element
    """
    def __init__(self, hyperp=None):
        if hyperp is not None:
            self.tau_max = hyperp.tau_max
        else:
            self.tau_max = 12

    # gamma enrichment ------------------------------------------------
    def gamma(self, R, t, param):
        """
        radial metallicity profile: straight line
        time dependency: f(t) = (1-t/tmax)**gamma

        Parameters
        ----------
        R: array
            galactocentric radius [kpc]
        t: array
            age [Gyr] 
        param: object of model parameters
            param.Rs = radius of solar abundance [kpc]
            param.gamma = gamma exponent for time (0-1)
            param.grad = abundance gradient (dex/kpc)

        Returns
        -------
        array
            [X/H](R, t)
        """
        Rs = param.Rs
        gamma = param.gamma
        grad = param.grad
        Fm1 = param.Fm

        tau_max = self.tau_max
        f = Fm1 - (Fm1 + grad*Rs)*(1 - t/tau_max)**gamma + grad*R
        return f

    # broken line profile ------------------------------------------------
    def broken(self, R, t, pm):
        """
        Radial metallicity profile: broken line
        time dependency: f(t) = (1-t/tmax)**gamma

        Parameters
        ----------
        R: array
            galactocentric radius [kpc]
        t: array
            age [Gyr]
        pm: class instance of model parameters

        Returns
        --------
        array
            [X/H](R, t)

        Notes
        ------
        Implemented in Frankel+20

        .. math:: fe = fe_\mathrm{max}f(\\tau ) + b_{fe} + \\nabla(L_{z0})  \\frac{L_{z0}}{235~\mathrm{kms^{-1}}}

        .. math:: f(\\tau ) = \left( 1 - \\frac{\\tau}{12~\mathrm{Gyr}} \\right)^\gamma


        """
        a1 = pm.grad_in
        a2 = pm.grad
        gamma = pm.gamma
        b = pm.Femax*(1 - t/12)**gamma
        if isinstance(R, float) or isinstance(R, int) or isinstance(R, np.int64):
            if R < pm.Rbreak:
                y = a1*R + b
            elif R >= pm.Rbreak:
                y = a2*R + (a1 - a2)*pm.Rbreak + b
        elif isinstance(R, np.ndarray):
            y = a1*R + b
            y[R >= pm.Rbreak] = a2*R[R >= pm.Rbreak] \
                       + (a1 - a2)*pm.Rbreak + b[R >= pm.Rbreak] 
        
        return y



    # exponential enrichment --------------------------------------------
    def expo(self, R, t, param):
        """
        radial metallicity profile = straight line
        time dependency: exponential

        Parameters
        ----------
        R: array
            Galactocentric radius [kpc]
        t: array
            age [Gyr]
        param: class instance of model parametres
            param.Rs = radius if solar abundances [kpc]
            param.t_el = present-day enrichment time-scale [Gyr]
            param.grad = abundance gradient [dex/kpc]

        Returns
        --------
        array of length len(R) or len(t) 
            [X/H] values
        """
        Rs = param.Rs
        t_el = param.t_el
        grad = param.grad
        Fm1 = param.Fm
        tau_max = self.tau_max
        f_tau = (np.exp(-(tau_max - t)/t_el) - 1)\
                      /(np.exp(-tau_max/t_el) - 1)
        f = Fm1 - (Fm1 + grad*Rs)*f_tau + grad*R
        return f

    # Weinberg chemical enrichment ---------------------------------------
    def Weinberg(self, R, t, param, out_sfr=False):
        """
        Weinberg's chemical enrichment model
        
        Parameters
        ----------
        R: array
            Galactocentric radius [kpc]
        t: array
            age [Gyr]
        param = class instance of model parameters
            param.tsfr = star formation timescale [Gyr]
            param.eta  = mass loading factor

        Returns
        --------
            two arrays of length len(R) or len(t) 
        [Fe/H], [O/H]
        """
        eta = param.eta_a*R + param.eta_b
        t_sfr = param.tsfr + param.x*R/8

        case = 'exp'   # prescription (only 'exp' works for now)
        tauStar = 1    # star formation efficiency timescale Gyr
        tdmin = 0.15   # minimum delay time for SNIa Gy

        output = chem_waf_func(t, eta, tauStar, tdmin, t_sfr, case=case,
           mocc=0.015, mfecc=0.0015, mfeIa=0.0013, r=0.4,
           SolarO=0.0056, SolarFe=0.0012, dtout=0.020,
           tmax=11.0) 

        OH = output[:,1]
        FeH = output[:,2]
        sfr = output[:,4]

        if out_sfr:
             return FeH, OH, sfr
        else:
             return FeH, OH

    # Add your chemical enrichment model here ---------------------------------------


    # Inverse relation for chemical enrichment --------------------------------------
    def R0_element(self, fe, t, param):
        """
        finds birth radius through zeak chemical tagging:
        given metallicity and age --> R0

        Parameters
        ----------
        fe: array
            metallicity
        t: array
           age (Gyr)
        param: enrichment_parameters object
           model parameters

        Returns
        ---------
        array
            R0 birth radius

        Notes
        ------

        History:

        2019-06-03 frankel
        """
        # unpack model parameters
        Fm = param.Fm
        Rs, gam, grad = param.Rs, param.gamma, param.grad

        # find birth radius
        R0 = (fe - Fm + (Fm + grad*Rs)*(1 - t/12)**gam)/grad

        return R0

    def R0_break(self, fe, t, pm):
        """
        finds birth radius through weak chemical tagging:
        given metallicity and age --> R0

        Parameters
        ----------
        fe: array
            metallicity
        t: array
            age
        param: enrichment_parameters object

        Returns
        -------
        array of the same size as fe or t
            R0 (kpc), birth radius

        Notes
        ------

        History:

        2019-08-02 frankel
        """
        a1 = pm.grad_in
        a2 = pm.grad
        gamma = pm.gamma
        b = pm.Femax*(1 - t/12)**gamma

        # find metallicity at break given t and model
        fe_break = a1*pm.Rbreak + b

        # is it inner or outer disk
        if isinstance(fe, float) or isinstance(fe, int):
            if fe <= fe_break:
                # this is outer disk
                R0 = (fe - (a1 - a2)*pm.Rbreak - b)/a2
            elif fe > fe_break:
                R0 = (fe - b)/a1
        elif isinstance(fe, np.ndarray):
             # outer disk
             R0 = (fe - (a1 - a2)*pm.Rbreak - b)/a2
             # inner disk
             R0[fe > fe_break] = (fe[fe > fe_break] - b[fe > fe_break])/a1

        return R0

    def R0_Weinberg(self, fe, t, pm, ytol=0.1):
        """
        finds approximate birth radius through Newton-Raphson of the
        Weinberg method, to a precision in ytol (in dex in [Fe/H]).
        """
        # Guess birth radius: 8 kpc
        x_guess = 8*np.ones(len(t))
        y_root = fe

        # radial step in kpc to take gradient
        dx = 1

        # Solve for birth radius
        R0 = uf.Newton_Raphson(lambda ro: el.Weinberg(ro, t, pm_enrich)[0], 
                x_guess, y_root, dx, ytol)

        return R0

    # Add your R0_new_enrichment here --------------------------------------------


    #-----------------------------------------------------------------------------


#=======================================================================================================
def p_migration_drift(Lz, t, L0, vc0, pm):
    """
    Distribution of angular momentum a time t after birth at Lz0 due to stellar
    radial migration

    Parameters
    -----------
    Lz: array
        current angular momentum [kpc km/s]
    t: array
        time or age [Gyr]
    Lz0: array
        angular momentum t time ago
    pm: model parameters class instance
        need pm.srm: radial migration strength [kpc km/s]
    vc0: array
        circular velocity [km/s]

    Returns
    -------
    array
        pdf of (Lz | Lz0, t)

    Notes
    -----
    Implements radial migration

    .. math:: p(L_z | L_{z0}, \sigma_{Lzo}, \\tau) \propto \exp{\left(-\\frac{(L_z - L_{zo0} - D^{(1)}\\tau)^2}{2\sigma^2(\\tau)}\\right)}
    """

    # negative stuff
    Lz0 = L0 +0

    if isinstance(Lz0, np.ndarray):
        m = Lz0 <= 0
        Lz0[m] = 0.001

    # compute things
    sigma_t = pm.srm*(t/tau_max)**pm.beta_srm
    D = -pm.c4*(pm.srm)**2/(2*vc0*pm.rd*12)                       # drift term depending on scale-length
    c = 2*scp.erf(1 + scp.erf((Lz0 + D*t)/(np.sqrt(2)*sigma_t)))  # normalization
    y = np.exp(-(Lz - Lz0 - D*t)**2/(2*sigma_t**2))/(np.sqrt(2*np.pi)*sigma_t) 

    if isinstance(Lz0, np.ndarray):
        y[m] = 1e-50

    return y/c

def p_migration_radius(R, t, R0, pm):
    """
    distribution of angular momentum a time t after birth at Lz0

    arguments:
    Lz current angular momentum [kpc km/s]
    t time or age [Gyr]
    Lz0 angular momentum t time ago
    sigma: radial migration strength [kpc km/s]
    vc0 circular velocity [km/s]
    Rd = disk scale length [kpc]

    returns: pdf of (Lz | Lz0, t)
    """
    sigma_t = pm.srm*np.sqrt(t/tau_max)         # diffusion
    D = -pm.c4*(pm.srm)**2/(2*pm.rd*12)            # drift term depending on scale-length
    c = 2*scp.erf(1 + scp.erf((R0 + D*t)/(np.sqrt(2)*sigma_t)))  # normalization
    y = np.exp(-(R - R0 - D*t)**2/(2*sigma_t**2))/(np.sqrt(2*np.pi)*sigma_t)
    return y/c

#========================================================================================================


#========================================================================================================
# in plane heating
#========================================================================================================

#-----------------------------------------------------------------
# velocity dispersion in the radial direction
#-----------------------------------------------------------------
def vel_disp_r(Rc, t, pm):
    """
    velocity dispersion as a function of time and guiding radius
    increases with age (heating) as about t**0.33
    and decays exponentially radially
    Binney 2012 quasi isothermal distribution functions

    Parameters
    ------------
    Rc: array
        guiding radius of the orbit (kpc)
    t: array
        stellar ages (time after birth)
    pm: model parameters class instance 
        the important ones are
         pm.vel_disp_r0  (km/s) ~ vel disp at solar radius
         pm.Rd_heating   (kpc) decline of velocity dispersion
         pm.R0           (kpc) solar radius
         pm.t1           (Gyr) 0.11
         pm.tt
         pm.beta         power law time evolution 0.33

    Returns
    -------
    the velocity dispersion at that place and time (km/s)
    """
    y = pm.sigma_vr0*np.exp((pm.R8 - Rc)/pm.Rd_heating)
    y1 = y*((t + pm.t1)/(pm.t_T + pm.t1))**pm.beta
    return y1

#---------------------------------------------------------------
# epicycle frequency
#---------------------------------------------------------------
def epicycle_frequency(Rc):
    """
    radial oscillation frequency
    
    Parameters
    -----------
    Rc: array
        guiding radius (kpc)
    
    Returns
    -------
    the epicycle frequency

    comments: uses Galpy
    """
    k = epifreq(pot, Rc/_REFR0)*_REFV0/_REFR0
    return k

#---------------------------------------------------------------
# rotation frequency (conjugate to p_phi)
#---------------------------------------------------------------
def calculate_omega(Rc):
    """
    Oscillations in the azimuth

    Parameters
    ----------
    Rc: array
        guiding radius (kpc)

    Returns
    --------
    omega: the orbital frequency

    comments:
    uses Galpy
    """
    omega = omegac(pot, Rc/_REFR0)*_REFV0/_REFR0
    return omega

#-----------------------------------------------------------------
# Radial action distribution function
#-----------------------------------------------------------------
def p_heating(Jr, k, sigma_r):
    """
    Distribution of radial action due to radial heating

    Parameters
    ----------
    Jr: array
        radial action [kpc km/s]
    k: array
        epicycle freqency
    sigma_r: array
        velocity dispersion [km/s]

    Returns
    -------
    array
        value of the probability distribution function

    Notes
    -----
    Implements standard quasi-isothermal distribution functions (Binney), Frankel+20 eq.xx

    .. math:: p(J_R | \sigma_R, \kappa) = \\frac{1}{2\pi\sigma_R^2}\kappa\exp(-J_R\kappa/\sigma_R^2)

    History:
    
    2020 frankel

    """
    p = 1/(2*np.pi*sigma_r**2)*k*np.exp(-Jr*k/sigma_r**2)
    return p

#-----------------------------------------------------------------------------------
# circular velocity
def calculate_vcirc(R):
    """
    Calculate the circular velocity at Galactocentric radius R

    uses Galpy
    """
    y = vcirc(pot, R/_REFR0)*_REFV0
    return y

#def calculate_vdridt(R, vcirc, pm):


# find guiding radius of angular momentum Lz given a rotation curve
# how precisely do we need to know this?
def R_circ(Lz):
    """
    Interpolate the radius of angular momentum Lz
    in the potential used in this code

    uses Galpy
    """
    # interpolation grid: radius
    R_grid = np.linspace(0.001,35, 400)

    # interpolation grid: angular momentum
    L_grid = R_grid*vcirc(pot, R_grid/_REFR0)*_REFV0

    # interpolate on desired points
    return np.interp(Lz, L_grid, R_grid)


#=================================================================================
# vertical heating
#=================================================================================

#----------------------------------------------------------------
# vertical heating in action
#----------------------------------------------------------------
def vertical_action(R, t, pm=None):
    """
    Model for the mean vertical action for the Galactic disk,
    as a function of radius and time. From Ting and Rix (2019)

    Parameters
    -----------
    R: array
        Galactocentric radius (mean between birth and present day, kpc)
    t: array
        stellar age (Gyr)

    Returns
    --------
        <J_z>(R, t)

    Notes
    ------
    
    History

    2019-01-19 frankel
    2019-07-31  added possibility to fit params in pm
    """
    if pm is None:
        a0 = 1.05
        a1 = 0.55*1e-1
        b0 = 1.79
        b1 = 0.5*1e-1
        c0 = 0.91
        c1 = 1.83*1e-1
        c2 = 8.7e-2
        c3 = 14e-3
    else:
        a0 = pm.a0
        a1 = pm.a1
        b0 = pm.b0
        b1 = pm.b1
        c0 = pm.c0
        c1 = pm.c1
        c2 = pm.c2
        c3 = pm.c3    

    r = (R - 8)
    gamma = a0 + a1*r
    dJzdt = (b0 + b1*r)
    Jz0 = c0 + c1*r + c2*r*r + c3*r*r*r
    Jz = Jz0 + dJzdt * t**(gamma)
    if np.any(Jz <= 0):
        #print('Jz neg ', t[Jz <= 0], R[Jz <=0])
        Jz[Jz <= 0] = np.min(Jz[Jz >0])-1e-100
    return Jz

#--------------------------------------------------------------
# isothermal distribution, epicycle approximation
#--------------------------------------------------------------
def scale_height(R, R0, tau, az=1, pm=None):
    """
    Scale height in function of the mean vertical action
    and the vertical frequency as a function of birth radius,
    current radius and time. For isothermal sech2 distribution

    Parameters
    ----------
    R: array
        present day Galctocentric radius [kpc]
    R0: array
        birth Galactocentric radius [kpc]
    t: array
        stellar age [Gyr]

    Returns
    --------
    array
        hz: the scale-height [kpc]

    Notes
    -----

    History

    2019-01-19 frankel
    """
    m = R0 < 0.0
    R01 = R0 + 0
    R01[m] = 0.000001
    jz = vertical_action((R+R01)/2., tau, pm=pm)

    if np.any(jz <= 0):
        print('Negative radial action in scale_height, rm_new.py')
    nu = verticalfreq(pot, R/_REFR0)*_REFV0/_REFR0
    hz = np.sqrt(2*jz/nu)
    if np.any(hz <= 0): print('Negative hz in scale_height, rm_new.py')
    return hz*az

# vertical distribution
def p_z(R, R0, tau, z, az=1, pm=None):
    """
    Vertical isothermal distribution for the Galactic disk
    as a function of Galactocentric radius and age. Based on
    Ting and Rix analysis of the vertical heating history
    of the Galactic disk.

    Parameters
    -----------
    R: array
       present-day radius (kpc)
    
    R0: array
       birth radius (kpc)
    
    z: array
        height above the Galactic plane (kpc)
    
    h: array
        scale_height(R, R0, tau), kpc
        return sech2(z/h)/(2*h)

    az: float
        default: 1
        scaling factor to calibrate between Ting's and Frankel's models


    Returns
    --------
    array
        p_z(z | R0, R, t) = proba of being at height z given
                            the R0, R and age of a star


    Notes
    ------
    Implements isothermal sech2 distribution

    .. math:: p_z(z | R_0, R, t) = sech2(z/h_z(R,R_0,\\tau))/(2*h_z(R,0,\\tau))

    """
    h = scale_height(R, R0, tau, az=az, pm=pm)
    return sech2(z/h)/(2*h)



#=================================================================================
# calculating actions and potential stuff using Galpy
#=================================================================================

# Define potential
aAS = actionAngleStaeckel(
        pot   = pot,        #potential
        delta = 0.45,       #focal length of confocal coordinate system
        c     = True        #use C code (for speed)
        )


# compute the actions
def compute_actions(R, z, vr, vphi, vz):
    """
    Compute radial, azimuthal, vertical actions given the potential
    defined in this code.

    arguments
    R = Galactocentric radius [kpc]
    z = height above the plane
    vr = velocity in Galactocentric radius direction [km/s]
    vphi = velocity in azimuthal direction [km/s]
    vz = vertical velocity [km/s]

    returns
    Jr, Jphi, Jz
    """
    jR, lz, jz = aAS(R/_REFR0, vr/_REFV0,
        vphi/_REFV0, z/_REFR0, vz/_REFV0)
    return [jR*_REFR0*_REFV0, lz*_REFR0*_REFV0, jz*_REFR0*_REFV0]


def p_vphi_from_actions(vphi, vr, R, t, pm):
    vz = np.zeros(len(vr))
    JR, LZ, JZ = compute_actions(R, np.zeros(len(vr)), vr, vphi, vz)
    Rcirc = R_circ(LZ)
    k = epicycle_frequency(Rcirc)
    sigma_r = vel_disp_r(Rcirc, t, pm)
    p_heat= p_heating(JR, k, sigma_r) #+ 1e-500
    return p_heat







