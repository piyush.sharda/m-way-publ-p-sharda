.. rm_new documentation master file, created by
   sphinx-quickstart on Sat Apr 17 15:48:28 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MilpyWay's documentation!
==================================


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   installation.rst
   intro.rst
   reference.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
